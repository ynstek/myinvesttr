//
//  MIFirebase.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 25.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import FirebaseMessaging

final public class MIFirebase: NSObject {

	func firebaseSetTokenAndTopic(_ tokenId: String? = nil) {
		// TODO: Firebase - Abone olma uygulama acildiktan sonra yapilmali. yoksa null veriyor ve abone olamiyor.
		
		if let modelData = UserDefaults.standard.object(forKey: "LoginResponseModel") as? Data,
			let loginResponseModel = try? PropertyListDecoder().decode(LoginResponseModel.self, from: modelData) {
			
			for topic in loginResponseModel.notificationChannels ?? [] {
				Messaging.messaging().subscribe(toTopic: topic.name ?? "")
				print("Subscribed to topic:", topic.name ?? "")
			}
		}
		let token = tokenId != nil ? tokenId! : Messaging.messaging().fcmToken ?? ""
		print("FCM token: \(token)")
		
		//		JsonUpdateCustomer.Todo.token(tokenid: token) { (error) in
		//			// TokenId is Posted
		//		}
	}
}
