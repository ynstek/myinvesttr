//
//  AppDelegate.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 6.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import AlamofireNetworkActivityLogger
import Firebase
import FirebaseMessaging
import IQKeyboardManager
import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	let gcmMessageIDKey = "gcm.message_id" // firebase

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		
		// Main
		self.prepareMain()
		
		// Alamofire Log
		self.startAlamofireLogger()
		
		// IQKeyboard
		self.configureIQKeyboard()
		
		// Firebase
		self.createFirebase(application)
		
		return true
	}

	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}

	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}
	
	// MARK: Helper
	
	private func prepareMain() {
		self.window = UIWindow(frame: UIScreen.main.bounds)
		let storyboardName = MIConfiguration().configForKey("ROOT_STORYBOARD_NAME") as! String
		let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
		
		let initialViewController = storyboard.instantiateInitialViewController()
		
		self.window?.rootViewController = initialViewController
		self.window?.makeKeyAndVisible()
	}
	
	private func startAlamofireLogger() {
		NetworkActivityLogger.shared.startLogging()
		NetworkActivityLogger.shared.level = .debug
	}
	
	func configureIQKeyboard() {
		IQKeyboardManager.shared().isEnabled = true
		IQKeyboardManager.shared().enableDebugging = true
		
		// Done butonu
		IQKeyboardManager.shared().isEnableAutoToolbar = true
		IQKeyboardManager.shared().toolbarDoneBarButtonItemText = "Bitti"
		
		// Klavyenin texte uzakligi
		IQKeyboardManager.shared().keyboardDistanceFromTextField = 10
		
		// Place Holderlari goster
		IQKeyboardManager.shared().shouldShowToolbarPlaceholder = true
		IQKeyboardManager.shared().placeholderFont = MITheme().font("fontRegular")
		
		// Bos yere tiklayinca klavyeyi kapat
		IQKeyboardManager.shared().shouldResignOnTouchOutside = true
		
//		IQKeyboardManager.shared().shouldPlayInputClicks = false // ?
	}

}

extension AppDelegate : MessagingDelegate, UNUserNotificationCenterDelegate {
	
	// MARK: Firebase
	func createFirebase(_ application: UIApplication) {
		// Override point for customization after application launch.
		
		// register_for_notifications
		if #available(iOS 10.0, *) {
			// For iOS 10 display notification (sent via APNS)
			UNUserNotificationCenter.current().delegate = self
			
			let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound ]
			
			UNUserNotificationCenter.current().requestAuthorization(
				options: authOptions,
				completionHandler: {_, _ in })
		} else {
			let settings: UIUserNotificationSettings =
				UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
			application.registerUserNotificationSettings(settings)
		}
		application.registerForRemoteNotifications() // if else ten sonra olmali.
		
		// MARK: Firebase Configure
		FirebaseApp.configure()
		
		// Uygulama acikken bildirim gelmesini saglar
		let current = UNUserNotificationCenter.current()
		current.delegate = self
		
		// Uygulama bildirim sayisini sifirlama
		application.applicationIconBadgeNumber = 0
		current.removeAllDeliveredNotifications() // To remove all delivered notifications
		current.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
		
		// [START set_messaging_delegate]
		Messaging.messaging().delegate = self //as? MessagingDelegate
		// [END set_messaging_delegate]
	}
	
	func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
		print("Failed. Unable to register for remote notifications: \(error.localizedDescription)")
	}
	
	// uygulama acikken bildirimi yakalar
	@available(iOS 10.0, *)
	func userNotificationCenter(_ center: UNUserNotificationCenter,
								willPresent notification: UNNotification,
								withCompletionHandler completionHandler: @escaping (_ options: UNNotificationPresentationOptions) -> Void) {
		
		completionHandler([.badge, .alert, .sound])
	}
	
	// MARK: [START receive_message]
	func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
		// If you are receiving a notification message while your app is in the background,
		// this callback will not be fired till the user taps on the notification launching the application.
		// TODO: Handle data of notification
		// With swizzling disabled you must let Messaging know about the message, for Analytics
		Messaging.messaging().appDidReceiveMessage(userInfo)
		// Print message ID.
		
		if let messageID = userInfo[gcmMessageIDKey] {
			print("Message ID: \(messageID)")
		}
		
		application.applicationIconBadgeNumber += 1
		print(userInfo)
	}
	
	// MARK: Bildirime tiklaninca
	func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
					 fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
		// If you are receiving a notification message while your app is in the background,
		// this callback will not be fired till the user taps on the notification launching the application.
		// TODO: Handle data of notification
		// With swizzling disabled you must let Messaging know about the message, for Analytics
		// Messaging.messaging().appDidReceiveMessage(userInfo)
		// Print message ID.
		
		if let messageID = userInfo[gcmMessageIDKey] {
			print("Message ID: \(messageID)")
		}
		
		let dict = userInfo["aps"] as! NSDictionary
		if let d : [String : Any] = dict["alert"] as? [String : Any] {
			let body : String = d["body"] as! String
			let title : String = d["title"] as! String
			print("Title:\(title)\nbody:\(body)")
		} else {
			let dict = userInfo["aps"] as! NSDictionary;
			print(dict)
			let message = dict["alert"];
			print("%@", message!);
		}
		
//		if let sellerid = userInfo["sellerid"] as? String {
//
//		}
		
		Messaging.messaging().appDidReceiveMessage(userInfo)
		completionHandler(UIBackgroundFetchResult.newData)
	}
	
	
	func application(application: UIApplication,
					 didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
		Messaging.messaging().apnsToken = deviceToken
	}
	
	// [START refresh_token]
	private func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
		print("Firebase registration token: \(fcmToken)")
		MIFirebase().firebaseSetTokenAndTopic(fcmToken)
	}
	
	// [END refresh_token]
	// [START ios_10_data_message]
	// Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
	// To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
	func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
		print("Received data message: \(remoteMessage.appData)")
	}
	// [END ios_10_data_message]
}
