//
//  MIService+Presenters.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 16.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

extension MIService {
	func showErrorAlert(_ error: Any?) {
		
		var errorTitle = "" // "Beklenmeyen bir hata oluştu!"
		let errorMessage: String
		
		// Create service error
		if let error = error as? NSError {
			debugPrint(error.debugDescription)
			let errorCode = error.code
			
			if errorCode == NSURLErrorTimedOut {
				// ServiceTimeoutMessage
				errorMessage = "Sunucudan uzun bir süre boyunca cevap gelmediği için istek sonlandırıldı."
			} else if errorCode == NSURLErrorNotConnectedToInternet
				|| errorCode == NSURLErrorNetworkConnectionLost {
				// NetworkCommonReachabilityError
				errorTitle = "İnternet bağlantınız yok."
				errorMessage = "İşleminiz gerçekleştirilemiyor. Lütfen bağlantı ayarlarınızı kontrol ediniz."
			} else if errorCode == NSURLErrorUnknown
				|| errorCode == NSURLErrorCancelled
				|| errorCode == NSURLErrorBadURL
				|| errorCode == NSURLErrorUnsupportedURL
				|| errorCode == NSURLErrorCannotFindHost
				|| errorCode == NSURLErrorCannotConnectToHost
				|| errorCode == NSURLErrorNetworkConnectionLost
				|| errorCode == NSURLErrorDNSLookupFailed
				|| errorCode == NSURLErrorHTTPTooManyRedirects
				|| errorCode == NSURLErrorResourceUnavailable
				|| errorCode == NSURLErrorRedirectToNonExistentLocation
				|| errorCode == NSURLErrorBadServerResponse
				|| errorCode == NSURLErrorUserCancelledAuthentication
				|| errorCode == NSURLErrorUserAuthenticationRequired
				|| errorCode == NSURLErrorZeroByteResource
				|| errorCode == NSURLErrorCannotDecodeRawData
				|| errorCode == NSURLErrorCannotDecodeContentData
				|| errorCode == NSURLErrorCannotParseResponse
				|| errorCode == NSURLErrorAppTransportSecurityRequiresSecureConnection
				|| errorCode == NSURLErrorFileDoesNotExist
				|| errorCode == NSURLErrorFileIsDirectory
				|| errorCode == NSURLErrorNoPermissionsToReadFile
				|| errorCode == NSURLErrorDataLengthExceedsMaximum
				|| errorCode == NSURLErrorSecureConnectionFailed
				|| errorCode == NSURLErrorServerCertificateHasBadDate
				|| errorCode == NSURLErrorServerCertificateUntrusted
				|| errorCode == NSURLErrorServerCertificateHasUnknownRoot
				|| errorCode == NSURLErrorServerCertificateNotYetValid
				|| errorCode == NSURLErrorClientCertificateRejected
				|| errorCode == NSURLErrorCannotLoadFromNetwork
				|| errorCode == NSURLErrorClientCertificateRequired {
				// ServiceCommonErrorMessage
				errorMessage = "Teknik nedenlerden dolayı işleminizi gerçekleştiremiyoruz. Lütfen tekrar deneyiniz."
			} else {
				errorMessage = error.localizedDescription
			}
		} else {
			debugPrint(error as Any)
			errorTitle = "Beklenmeyen bir hata oluştu!"
			errorMessage = (error as? String) ?? "Bilinmeyen bir nedenden dolayı işleminizi gerçekleştiremiyoruz. Lütfen tekrar deneyiniz."
		}
		
		MIAlertPresenter().alert(errorTitle, bodyMessage: errorMessage)
	}
}
