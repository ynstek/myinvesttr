//
//  ContactRequestModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 30.05.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final public class ContactRequestModel: MIRequestModel {
	var name: String?
	var surname: String?
	var phone: String?
	var message: String?

	override public func encode(to encoder: Encoder) throws {
		try super.encode(to: encoder)
		try encoder.encode(name, for: "name")
		try encoder.encode(surname, for: "surname")
		try encoder.encode(phone, for: "phone")
		try encoder.encode(message, for: "message")
	}
	
	required init(from decoder: Decoder) throws {
		try super.init(from: decoder)
		name = try decoder.decode("name")
		surname = try decoder.decode("surname")
		phone = try decoder.decode("phone")
		message = try decoder.decode("message")
	}
	
	override init() {
		super.init()
	}
}
