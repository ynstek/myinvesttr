//
//  LoginRequestModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 24.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final public class LoginRequestModel: MIRequestModel {

	override public func encode(to value: Encoder) throws {
		try super.encode(to: value)
	}
	
	required init(from decoder: Decoder) throws {
		try super.init(from: decoder)
	}
	
	override init() {
		super.init()
	}
}
