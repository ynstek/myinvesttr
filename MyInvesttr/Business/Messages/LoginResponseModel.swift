//
//  LoginResponseModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 24.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import Codextended

final public class LoginResponseModel: MIResponseModel {
	var firstname: String?
	var lastname: String?
	var notificationChannels: [NotificationChannelModel]?
	
	override public func encode(to encoder: Encoder) throws {
		try super.encode(to: encoder)
		try encoder.encode(firstname, for: "firstname")
		try encoder.encode(lastname, for: "lastname")
		try encoder.encode(notificationChannels, for: "notificationChannels")
	}
	
	required init(from decoder: Decoder) throws {
		try super.init(from: decoder)
		firstname = try decoder.decodeIfPresent("firstname")
		lastname = try decoder.decodeIfPresent("lastname")
		notificationChannels = try decoder.decodeIfPresent("notificationChannels")
	}
	
	override init() {
		super.init()
	}
}
