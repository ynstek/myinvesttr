//
//  MIRequestModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 24.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import Codextended

open class MIRequestModel: Codable {
	var email: String?
	var password: String?
	
	public func encode(to encoder: Encoder) throws {
		try encoder.encode(email, for: "email")
		try encoder.encode(password, for: "password")
	}
	
	required public init(from decoder: Decoder) throws {
		email = try decoder.decodeIfPresent("email")
		password = try decoder.decodeIfPresent("password")
	}
	
	init() {
	}
}
