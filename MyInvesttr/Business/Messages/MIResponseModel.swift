//
//  MIResponseModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 24.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import Codextended

open class MIResponseModel: Codable {

	var status: Bool?
	var errorMessage: String?
	
	public func encode(to encoder: Encoder) throws {
		try encoder.encode(status, for: "status")
		try encoder.encode(errorMessage, for: "errMessage")
	}
	
	required public init(from decoder: Decoder) throws {
		status = try decoder.decodeIfPresent("status")
		errorMessage = try decoder.decodeIfPresent("errMessage")
	}
	
	init() {
	}
}
