//
//  RegisterRequestModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 27.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final public class RegisterRequestModel: MIRequestModel {
	var phone: String?
	var firstName: String?
	var lastName: String?

	override public func encode(to encoder: Encoder) throws {
		try super.encode(to: encoder)
		try encoder.encode(phone, for: "phone")
		try encoder.encode(firstName, for: "firstName")
		try encoder.encode(lastName, for: "lastName")
	}
	
	required init(from decoder: Decoder) throws {
		try super.init(from: decoder)
		phone = try decoder.decode("phone")
		firstName = try decoder.decode("firstName")
		lastName = try decoder.decode("lastName")
	}
	
	override init() {
		super.init()
	}
}
