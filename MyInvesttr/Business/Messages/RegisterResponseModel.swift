//
//  RegisterResponseModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 27.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final public class RegisterResponseModel: MIResponseModel {
	
	override public func encode(to encoder: Encoder) throws {
		try super.encode(to: encoder)
	}
	
	required init(from decoder: Decoder) throws {
		try super.init(from: decoder)
	}
	
	override init() {
		super.init()
	}
}
