//
//  MIService.swift
//  BisesDemo
//
//  Created by Yunus TEK on 26.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import Alamofire
import Foundation

open class MIService: NSObject {
	
	// MARK: Types
	
	public typealias SuccessClosure<ResponseModel: Codable> = (_ model: ResponseModel) -> Void
	public typealias ErrorClosure = (_ error: Error?) -> Void

	// MARK: Properties
	
	public var indicatorStrategy: IndicatorStrategyResult!
	private let contentType = MIConfiguration().configForKey("NETWORKING_API_REQUEST_CONTENT_TYPE") as! String
	private let timeoutInterval = MIConfiguration().configForKey("NETWORKING_API_TIMEOUT_INTERVAL_FOR_REQUEST") as! TimeInterval
	private var serviceRequest: URLRequest!

	// MARK: Private Call Cunctions
	
	/**
	* @brief call with post request
	*/
	public func call<RequestModel: Codable, ResponseModel: Codable>
		(_ requestModel: RequestModel?,
		 suffixPath: String,
		 successClosure: @escaping SuccessClosure<ResponseModel>,
		 errorClosure: ErrorClosure?
		) {
		self.showIndicator()
		
		let urlComponents = MIConfiguration().apiUrlComponents(suffixPath: suffixPath)
		var request = try! URLRequest(url: urlComponents, method: .post)
		
		let jsonData = try? requestModel.encoded()
		request.httpBody = jsonData
		
		self.setupRequest(request)
		
		Alamofire.request(self.serviceRequest)
			.responseJSON { responseData in
				self.responseHandler(responseData: responseData, successClosure: successClosure, errorClosure: errorClosure)
		}
	}
	
	/**
	* @brief call with get request
	*/
	public func call<ResponseModel: Codable>
		(_ queryItems: [URLQueryItem]?,
		 suffixPath: String,
		 successClosure: @escaping SuccessClosure<ResponseModel>,
		 errorClosure: ErrorClosure?
		) {
		self.showIndicator()

		let urlComponents = MIConfiguration().apiUrlComponents(suffixPath: suffixPath, queryItems: queryItems)
		let request = try! URLRequest(url: urlComponents, method: .get)
		self.setupRequest(request)

		Alamofire.request(self.serviceRequest)
			.responseJSON { responseData in
				self.responseHandler(responseData: responseData, successClosure: successClosure, errorClosure: errorClosure)
		}
	}
	
	private func setupRequest(_ request: URLRequest) {
		var request = request
		request.timeoutInterval = self.timeoutInterval
		request.allHTTPHeaderFields = Alamofire.SessionManager.defaultHTTPHeaders
		request.addValue(self.contentType, forHTTPHeaderField: "Content-Type")
		let versionNumber = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? ""
		let buildNumber = Bundle.main.infoDictionary!["CFBundleVersion"] as? String ?? ""
		let clientVersion = versionNumber + "." + buildNumber
		request.addValue(clientVersion, forHTTPHeaderField: "X-ClientVersion")
		
		self.serviceRequest = request
	}

	private func responseHandler<Model: Codable>(responseData: DataResponse<Any>, successClosure: SuccessClosure<Model>, errorClosure: ErrorClosure?) {
		guard responseData.error == nil else {
			self.serviceErrorHandler(responseData.error, successClosure: errorClosure)
			return
		}
		if let data = responseData.data {
			do {
				let decoder = JSONDecoder()
				let todos = try decoder.decode(Model.self, from: data)
				self.hideIndicator()
				successClosure(todos)
			} catch let DecodingError.dataCorrupted(errorMessage) {
				self.serviceErrorHandler(errorMessage, successClosure: errorClosure)
			} catch let DecodingError.keyNotFound(key, context) {
				let errorMessage = "Key '\(key)' not found:\(context.debugDescription)\ncodingPath:\(context.codingPath)"
				self.serviceErrorHandler(errorMessage, successClosure: errorClosure)
			} catch let DecodingError.valueNotFound(value, context) {
				let errorMessage = "Value '\(value)' not found:\(context.debugDescription)\ncodingPath:\(context.codingPath)"
				self.serviceErrorHandler(errorMessage, successClosure: errorClosure)
			} catch let DecodingError.typeMismatch(type, context)  {
				let errorMessage = "Type '\(type)' mismatch::\(context.debugDescription)\ncodingPath:\(context.codingPath)"
				self.serviceErrorHandler(errorMessage, successClosure: errorClosure)
			} catch let jsonError {
				self.serviceErrorHandler(jsonError, successClosure: errorClosure)
			}
		} else {
			self.serviceErrorHandler(successClosure: errorClosure)
		}
	}
	
	private func serviceErrorHandler(_ error: Any? = nil, successClosure: ErrorClosure?) {
		self.hideIndicator()
		self.showErrorAlert(error)
		if let closure = successClosure {
			closure((error as? Error) ?? nil)
		}
	}
	
	private func showIndicator() {
		if self.indicatorStrategy != .hide {
			MIIndicatorPresenter().present()
		}
	}
	
	private func hideIndicator() {
		if self.indicatorStrategy != .hide {
			MIIndicatorPresenter().hide()
		}
	}
}
