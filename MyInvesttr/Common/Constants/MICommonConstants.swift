//
//  MICommonConstants.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 24.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

public struct MICommonConstants {
	
	// MARK: Date Formats
	public static let defaultDisplayDateTimeLongFormat = "dd MMMM yyyy HH:mm"
	public static let navigationBackgroundViewTag = 1011

//	public static let defaultDisplayDateFormat = "dd.MM.yyyy"
//	public static let defaultDisplayDateTimeFormat = "dd.MM.yyyy HH:mm"
//	public static let defaultDisplayYearFormat = "YYYY"
//	public static let defaultDisplayDateLongFormat = "dd MMMM yyyy"
//	public static let defaultDisplayMonthShortFormat = "MM/yyyy"
//	public static let defaultDisplayTimeLongFormat = "HH:mm:ss"
//	public static let defaultDisplayTimeShortFormat = "HH:mm"
//	public static let displayDateDashFormat = "yyyy-MM-dd"
//	public static let displayDateSlashShortFormat = "MM/yy"
//	public static let shortDisplayDateFormat = "d.M.yy"
}
