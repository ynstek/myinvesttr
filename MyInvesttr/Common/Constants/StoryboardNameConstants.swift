//
//  StoryboardNameConstants.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 6.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

enum StoryboardNameConstants: String {
	case Account = "Account"
	case Analysis = "Analysis"
	case Article = "Article"
	case ArticleList = "ArticleList"
	case WebView = "WebView"
	case Main = "Main"
	case Market = "Market"
	case Menu = "Menu"
	case News = "News"
	case NewsDetail = "NewsDetail"
	case NewsList = "NewsList"
}
