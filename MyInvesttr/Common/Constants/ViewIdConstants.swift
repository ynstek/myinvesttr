//
//  ViewIdConstants.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 6.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

public enum ViewIdConstants: String {
	case Analysis = "AnalysisViewController"
	case AnalysisTableViewCell = "AnalysisTableViewCell"
	case ArticleList = "ArticleListViewController"
	case ArticleDetail = "ArticleDetailViewController"
	case ArticleTableViewCell = "ArticleTableViewCell"
	case Login = "LoginViewController"
    case Main = "MainTabBarController"
	case MarketCategory = "MarketCategoryViewController"
	case MarketDetail = "MarketDetailViewController"
	case MarketHeaderTableViewCell = "MarketHeaderTableViewCell"
	case MarketTableViewCell = "MarketTableViewCell"
	case MenuTableViewCell = "MenuTableViewCell"
	case NewsList = "NewsListViewController"
	case NewsDetail = "NewsDetailViewController"
	case NewsHeaderTableViewCell = "NewsHeaderTableViewCell"
	case NewsTableViewCell = "NewsTableViewCell"
	case Register = "RegisterViewController"
}
