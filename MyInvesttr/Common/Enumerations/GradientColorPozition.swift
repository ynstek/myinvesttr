//
//  GradientColorPozition.swift
//  BisesDemo
//
//  Created by Yunus TEK on 6.03.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit

enum GradientColorPozition: Int {
	case topToBottom = 0, letfToRight, bottomToTop, rightToLeft
}
