//
//  Application+Accessory.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 6.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

extension UIApplication {
	func topMostViewController() -> UIViewController? {
		return self.keyWindow?.rootViewController!.topMostViewController()
	}
}
