//
//  DataExtensions.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 23.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

extension Data {
	var html2AttributedString: NSAttributedString? {
		do {
			let attrString = try NSAttributedString(data: self , options: [
				.documentType: NSAttributedString.DocumentType.html,
				.characterEncoding: String.Encoding.utf8.rawValue
				], documentAttributes: nil)
			
			return attrString
		} catch {
			print("Error:", error)
			return nil
		}
	}
	var html2String: String {
		return html2AttributedString?.string ?? ""
	}
}
