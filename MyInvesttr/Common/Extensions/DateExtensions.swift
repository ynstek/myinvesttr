//
//  DateExtensions.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 24.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

extension Date {
	func toString(format: String?, locale: Locale? = nil) -> String? {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = format ?? ""
		if let locale = locale {
			dateFormatter.locale = locale
		}
		return dateFormatter.string(from: self)
	}
	
	func date(byAddingSeconds seconds: Int) -> Date? {
		let calendar = Calendar.current
		var components = DateComponents()
		components.second = seconds
		return calendar.date(byAdding: components, to: self)
	}
}
