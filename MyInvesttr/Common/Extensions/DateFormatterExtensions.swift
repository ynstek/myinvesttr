//
//  DateFormatter+Helper.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 22.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

extension DateFormatter {
	static let iso8601Full: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
		formatter.calendar = Calendar(identifier: .iso8601)
		formatter.timeZone = TimeZone(secondsFromGMT: 0)
		formatter.locale = Locale(identifier: "tr_TR")
		return formatter
	}()
}
