//
//  DoubleExtensions.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 31.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

extension Double {
	func toString(_ count: Int = 4) -> String {
		let format = "%.\(count)f"
		return String(format: format, self)
	}
}
