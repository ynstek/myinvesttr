//
//  String+Extensions.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 23.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

extension String {	
	var html2AttributedString: NSAttributedString? {
		let format = "%@<style>body,h1,h2,h3,h4,h5,h6,p,span,b,strong,i{font-family:'%@'!important;font-size:%fpx!important;}</style>"
		let font = MITheme().font("fontRegular")
		let dataWithFormat = String(format: format, self, font.fontName, font.pointSize)
		let data = dataWithFormat.data(using: .utf8, allowLossyConversion: true)
		
		return data?.html2AttributedString
	}
	var html2String: String {
		return html2AttributedString?.string ?? ""
	}
	
	func isValidEmail() -> Bool {
		let emailRegEx =
			"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
				+ "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
				+ "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
				+ "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
				+ "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
				+ "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
				+ "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
		
		let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
		
		let result = emailTest.evaluate(with: self)
		
		if result == false {
			MIAlertPresenter().alert("Email Geçerli Değil!", bodyMessage: "Lütfen geçerli bir Email adresi giriniz.")
		}
		
		return result
	}
	
	func isValidPhone(_ error: Bool = true) -> Bool {
//		let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
		let PHONE_REGEX = "^[0-9]{6,14}$"
		let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
		var result =  phoneTest.evaluate(with: self)
		
		//        if value.count != 11 {
		//            result = false
		//        }
		
		if Int(self) == 0 {
			result = false
		}
		
		if result == false && error {
			MIAlertPresenter().alert("Telefon Numarası Geçerli Değil!", bodyMessage: "Lütfen geçerli bir Telefon Numarası giriniz.")
		}
		
		return result
	}
}
