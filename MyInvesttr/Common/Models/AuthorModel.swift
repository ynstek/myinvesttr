//
//  AuthorModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 22.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

struct AuthorModel: Codable {
	var authorName : String?
	var authorAvatarUrl : String?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		authorName = try values.decodeIfPresent(String.self, forKey: .authorName)
		authorAvatarUrl = try values.decodeIfPresent(String.self, forKey: .authorAvatarUrl)
		let imageUrl = MIConfiguration().configForKey("IMAGE_URL") as! String
		authorAvatarUrl = imageUrl + "/" + (authorAvatarUrl ?? "")
	}
}
