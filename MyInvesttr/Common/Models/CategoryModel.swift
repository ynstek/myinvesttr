//
//  CategoryModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 16.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

struct CategoryModel: Codable {
	var name : String?
	var key: String?
}
