//
//  ChartModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 30.05.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

struct ChartModel: Codable {
	var q : String?
	var q1 : String?
	
	init(from decoder: Decoder) throws {
		q = try decoder.decodeIfPresent("q")
		q1 = try decoder.decodeIfPresent("q1")
	}
}
