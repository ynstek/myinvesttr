//
//  DisplayGroupsModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 16.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

struct DisplayGroupsModel: Codable {
	var homeBanner : Int?
	
	enum CodingKeys: String, CodingKey {
		case homeBanner = "HomeBanner"
	}
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		homeBanner = try values.decodeIfPresent(Int.self, forKey: .homeBanner)
	}
}
