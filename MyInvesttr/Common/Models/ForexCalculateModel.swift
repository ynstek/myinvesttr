//
//  ForexCalculateModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 14.06.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

public struct ForexCalculateModel: Codable {
	var name : String?
	var change_pct : Double?
	var end_rate : Double?
	var start_rate : Double?
	var change : Double?

	public init(from decoder: Decoder) throws {
		name = try decoder.decodeIfPresent("name")
		change_pct = try decoder.decodeIfPresent("change_pct")
		end_rate = try decoder.decodeIfPresent("end_rate")
		start_rate = try decoder.decodeIfPresent("start_rate")
		change = try decoder.decodeIfPresent("change")
	}
}
