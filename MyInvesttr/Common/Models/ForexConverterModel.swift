//
//  ForexConverterModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 17.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

struct ForexConverterModel: Codable {
	var flagName : String?
	var descriptionName : String?
	var name : String?
}
