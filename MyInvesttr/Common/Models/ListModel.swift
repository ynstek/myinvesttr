//
//  ListModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 16.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

public struct ListModel: Codable {
	var displayGroups: DisplayGroupsModel?
	var url: String?
	var publishDate: Date?
	var author: AuthorModel?
	var tags: [TagModel]?
	var categories: [CategoryModel]?
	var properties: PropertyModel?
	
	public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		displayGroups = try values.decodeIfPresent(DisplayGroupsModel.self, forKey: .displayGroups)
		url = try values.decodeIfPresent(String.self, forKey: .url)
		let publishDateString = try values.decodeIfPresent(String.self, forKey: .publishDate)
		publishDate = DateFormatter.iso8601Full.date(from: publishDateString ?? "")
		author = try values.decodeIfPresent(AuthorModel.self, forKey: .author)
		tags = try values.decodeIfPresent([TagModel].self, forKey: .tags)
		categories = try values.decodeIfPresent([CategoryModel].self, forKey: .categories)
		properties = try values.decodeIfPresent(PropertyModel.self, forKey: .properties)
	}
}
