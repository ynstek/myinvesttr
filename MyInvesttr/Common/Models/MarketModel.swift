//
//  MarketModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 31.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

public struct MarketModel : Codable {
	let name : String?
	let symbol : String?
	let symbolId : String?
	let securityDesc : String?
	let high : String?
	let bid : Double?
	let last : Double?
	let dailyChange : Double?
	let dailyChangePercent : Double?
	var imageUrl : String?
	var chart: ChartModel?
	
	public init(from decoder: Decoder) throws {
		name = try decoder.decodeIfPresent("name")
		symbol = try decoder.decodeIfPresent("symbol")
		bid = try decoder.decodeIfPresent("Bid")
		last = try decoder.decodeIfPresent("Last")
		dailyChange = try decoder.decodeIfPresent("DailyChange")
		dailyChangePercent = try decoder.decodeIfPresent("DailyChangePercent")
		symbolId = try decoder.decodeIfPresent("symbolId")
		securityDesc = try decoder.decodeIfPresent("securityDesc")
		high = try decoder.decodeIfPresent("High")
		chart = try decoder.decodeIfPresent("Chart")
		imageUrl = try decoder.decodeIfPresent("imageUrl")
		if imageUrl != nil && URL(string: imageUrl ?? "")?.scheme == nil {
			imageUrl = "https://" + imageUrl!
		}
	}
}
