//
//  MenuModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 25.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

struct MenuModel {
	var title : String?
	var imageName : String?
	var identifier: MenuSegueIdentifier?
	
	init(title: String, imageName: String, identifier: MenuSegueIdentifier?) {
		self.title = title
		self.imageName = imageName
		self.identifier = identifier
	}
}
