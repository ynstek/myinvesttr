//
//  NotificationChannelModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 7.05.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

struct NotificationChannelModel: Codable {
	let name : String?
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		name = try values.decodeIfPresent(String.self, forKey: .name)
	}
}
