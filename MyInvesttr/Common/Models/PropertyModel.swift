//
//  PropertyModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 16.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

struct PropertyModel : Codable {
	var title : String?
	var imagePath : String?
	var content : String?
	var summary : String?
	var videoPath : String?
	var symbol : String?
	var currentPrice : String?
	var option1 : String?
	var option2 : String?
	var takeProfit : String?
	var stopLoss : String?
	var videoThumb : String?
	var process : String?
	
	enum CodingKeys: String, CodingKey {
		case title = "Title"
		case imagePath = "ImagePath"
		case content = "Content"
		case summary = "Summary"
		case videoPath = "VideoPath"
		case symbol = "Symbol"
		case currentPrice = "CurrentPrice"
		case option1 = "Option1"
		case option2 = "Option2"
		case takeProfit = "TakeProfit"
		case stopLoss = "StopLoss"
		case videoThumb = "VideoThumb"
		case process = "Process"
	}
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		imagePath = try values.decodeIfPresent(String.self, forKey: .imagePath)
		let imageUrl = MIConfiguration().configForKey("IMAGE_URL") as! String
		imagePath = imageUrl + "/" + (imagePath ?? "")
		content = try values.decodeIfPresent(String.self, forKey: .content)
		summary = try values.decodeIfPresent(String.self, forKey: .summary)
		videoPath = try values.decodeIfPresent(String.self, forKey: .videoPath)
		symbol = try values.decodeIfPresent(String.self, forKey: .symbol)
		currentPrice = try values.decodeIfPresent(String.self, forKey: .currentPrice)
		option1 = try values.decodeIfPresent(String.self, forKey: .option1)
		option2 = try values.decodeIfPresent(String.self, forKey: .option2)
		takeProfit = try values.decodeIfPresent(String.self, forKey: .takeProfit)
		stopLoss = try values.decodeIfPresent(String.self, forKey: .stopLoss)
		videoThumb = try values.decodeIfPresent(String.self, forKey: .videoThumb)
		process = try values.decodeIfPresent(String.self, forKey: .process)
	}
}
