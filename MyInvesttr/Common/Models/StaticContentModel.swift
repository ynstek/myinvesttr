//
//  StaticContentModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 15.05.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

public struct StaticContentModel: Codable {
	let key : String?
	let text : String?

	public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		key = try values.decodeIfPresent(String.self, forKey: .key)
		text = try values.decodeIfPresent(String.self, forKey: .text)
	}
}
