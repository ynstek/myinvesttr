//
//  TagModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 16.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

struct TagModel : Codable {
	var name : String?
	
	enum CodingKeys: String, CodingKey {
		case name = "name"
	}
}
