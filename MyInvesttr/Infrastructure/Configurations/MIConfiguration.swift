//
//  MIConfiguration.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 12.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final public class MIConfiguration: NSObject {
	
	// MARK: Constants
	
	let defaultConfigName = "default"
	
	// MARK: Config For Key
	
	public func configForKey(_ key: String) -> Any? {
		let path = Bundle.main.path(forResource: "Configuration", ofType: "json")!
		do {
			let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
			let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
			if let values = jsonResult as? Dictionary<String, AnyObject> {
				if let value = values[self.getConfigName()]![key], value != nil {
					return value
				} else if let value = values[self.defaultConfigName]![key] {
					return value
				}
			}
		} catch {
			return nil
		}
		return nil
	}
	
	/// Url with Version
	public func urlForKeyWithVersion(_ key: String) -> String {
		var url = self.configForKey(key) as! String
		url += "/" + (self.configForKey("NETWORKING_API_VERSION") as! String)
		return url
	}
	
	func getConfigName() -> String {
		
		#if DEVELOPMENT
		let configurationKey = "Development"
		#elseif DUMMY
		let configurationKey = "Dummy"
		#elseif PRODUCTION
		let configurationKey = "Production"
		#else
		let configurationKey = self.defaultConfigName
		#endif
		
		return configurationKey
	}
	
	public func apiUrl() -> String {
		return self.configForKey("NETWORKING_API_URL") as! String
	}
	
	public func apiUrlComponents(suffixPath: String = "", queryItems: [URLQueryItem]? = nil) -> URLComponents {
		var urlComponents = URLComponents()
		urlComponents.scheme = self.apiScheme()
		urlComponents.host = self.apiHost()
		urlComponents.path = self.apiPath() + suffixPath
		urlComponents.queryItems = queryItems
		
		return urlComponents
	}
	
	// MARK: Private
	
	private func apiScheme() -> String {
		return self.apiUrl().components(separatedBy: "://")[0]
	}
	
	private func apiHost() -> String {
		return self.apiUrl().components(separatedBy: "/")[2]
	}
	
	private func apiPath() -> String {
		let seperator = "\(self.apiScheme())://\(self.apiHost())"
		return self.apiUrl().components(separatedBy: seperator)[1]
	}
}
