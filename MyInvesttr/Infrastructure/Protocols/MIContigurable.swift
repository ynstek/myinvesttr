//
//  MIContigurable.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 8.05.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

protocol MIContigurable: NSObjectProtocol {
	/**
	* @brief configuration.
	*/
	var configuration: MIConfiguration { get }
}
