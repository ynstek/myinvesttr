//
//  MIThemeable.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 2.05.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

protocol MIThemeable: NSObjectProtocol {
	/**
	* @brief theme.
	*/
	var theme: MITheme { get }
}
