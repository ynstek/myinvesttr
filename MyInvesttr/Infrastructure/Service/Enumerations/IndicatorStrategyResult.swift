//
//  IndicatorStrategyResult.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 31.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

public enum IndicatorStrategyResult: Int {
	case show = 0
	case hide
}
