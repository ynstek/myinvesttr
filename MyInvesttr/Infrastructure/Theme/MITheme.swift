//
//  MITheme.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 16.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final public class MITheme: NSObject {
	
	public func color(_ key: String) -> UIColor {
		let key = key.hasSuffix("Color") ? key : key + "Color"
		let colorHex = self.contentsOfFile()[key] as! String
		return UIColor(colorHex)
	}
	
	public func font(_ key: String, _ size: CGFloat? = nil) -> UIFont {
		let key = key.hasPrefix("font") ? key : key + "font"
		let value = self.contentsOfFile()[key] as! String
		let fontSize = size != nil ? size! : CGFloat(Int(value.components(separatedBy: "px")[0])!)
		let fontName = String(value.components(separatedBy: "'")[1])
		return UIFont(name: fontName, size: fontSize)!
	}
	
	/**
	Gets the contents of the specified plist file.
	
	- parameter plistName: property list where defaults are declared
	- parameter bundle: bundle where defaults reside
	
	- returns: dictionary of values
	*/
	fileprivate func contentsOfFile(plistName: String = "Theme.plist", bundle: Bundle? = nil) -> [String : AnyObject] {
		let fileParts = plistName.components(separatedBy: ".")
		
		guard fileParts.count == 2,
			let resourcePath = (bundle ?? Bundle.main).path(forResource: fileParts[0], ofType: fileParts[1]),
			let contents = NSDictionary(contentsOfFile: resourcePath) as? [String : AnyObject]
			else { return [:] }
		
		return contents["Default"] as! [String : AnyObject]
	}
	
	/*
	
	/**
	Gets the contents of the specified bundle URL.
	
	- parameter bundleURL: bundle URL where defaults reside
	- parameter plistName: property list where defaults are declared
	
	- returns: dictionary of values
	*/
	fileprivate static func contentsOfFile(bundleURL: URL, plistName: String = "Root.plist") -> [String : AnyObject] {
		// Extract plist file from bundle
		guard let contents = NSDictionary(contentsOf: bundleURL.appendingPathComponent(plistName))
			else { return [:] }
		
		// Collect default values
		guard let preferences = contents.value(forKey: "PreferenceSpecifiers") as? [String: AnyObject]
			else { return [:] }
		
		return preferences
	}
	
	/**
	Gets the contents of the specified bundle name.
	
	- parameter bundleName: bundle name where defaults reside
	- parameter plistName: property list where defaults are declared
	
	- returns: dictionary of values
	*/
	fileprivate static func contentsOfFile(bundleName: String, plistName: String = "Root.plist") -> [String : AnyObject] {
		guard let bundleURL = Bundle.main.url(forResource: bundleName, withExtension: "bundle")
			else { return [:] }
		
		return contentsOfFile(bundleURL: bundleURL, plistName: plistName)
	}
	
	/**
	Gets the contents of the specified bundle.
	
	- parameter bundle: bundle where defaults reside
	- parameter bundleName: bundle name where defaults reside
	- parameter plistName: property list where defaults are declared
	
	- returns: dictionary of values
	*/
	fileprivate static func contentsOfFile(bundle: Bundle, bundleName: String = "Settings", plistName: String = "Root.plist") -> [String : AnyObject] {
		guard let bundleURL = bundle.url(forResource: bundleName, withExtension: "bundle")
			else { return [:] }
		
		return contentsOfFile(bundleURL: bundleURL, plistName: plistName)
	}

	*/
}
