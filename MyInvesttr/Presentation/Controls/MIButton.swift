//
//  MIButton.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 6.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

open class MIButton: UIButton {
	
	public required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.initialize()
	}
	
	public override init(frame: CGRect) {
		super.init(frame: frame)
		self.initialize()
	}
	
	func initialize() {
		self.backgroundColor = MITheme().color("greenDarkColor")
		self.tintColor = MITheme().color("whiteColor")
	}
}
