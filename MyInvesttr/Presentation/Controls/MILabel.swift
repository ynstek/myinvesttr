//
//  MILabel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 24.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

open class MILabel: UILabel {

	@IBInspectable
	public var fontName: String = ""
	@IBInspectable
	public var fontSize: CGFloat = 0

	override open func draw(_ rect: CGRect) {
		if self.fontName != "" {
			self.font = MITheme().font(self.fontName)
		}
		if self.fontSize != 0 {
			self.font = UIFont(name: self.font.fontName, size: self.fontSize)
		}
		super.draw(rect)
    }
}
