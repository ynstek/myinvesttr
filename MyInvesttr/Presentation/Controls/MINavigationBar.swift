
//
//  MINavigationBar.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 16.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class MINavigationBar: UINavigationBar {

	override func didMoveToWindow() {
		super.didMoveToWindow()
		self.prepareUI()
		
		// Check topItem exists
		let topItem = self.topItem as? MINavigationItem
		if self.superview != nil && topItem != nil {
			topItem?.applyAppearance()
		}
	}
	
	fileprivate func prepareUI() {
        // NavigationBar Color
        /*
         // Tab degistirince back button rengini clear yapiyor.
         let backgroundColor = MITheme().color("clearColor")
         self.tintColor = backgroundColor
         self.barTintColor = backgroundColor
         self.backgroundColor = backgroundColor
        */
        
        // Hide Bottom line
        self.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.shadowImage = UIImage()
        // StatusBar
        self.isTranslucent = true
    }
}
