//
//  MINavigationItem.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 16.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class MINavigationItem: UINavigationItem {
	
	@IBInspectable
	public var showLogo: Bool = false
	
	// MARK: Theming
	
	func applyAppearance() {
		// Add logo on navigationItem
		if self.titleView == nil && self.showLogo {
			let frame = CGRect(x: 0, y: 0, width: 150, height: 25)
			let image = UIImage(named: "logoWhite")
			let imageView = UIImageView(frame: frame)
			imageView.image = image
			imageView.contentMode = .scaleAspectFit
			
			let logoView = UIView(frame: frame)
			logoView.addSubview(imageView)
			
			self.titleView = logoView
		}
		
		// Back button title
		self.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
	}	
}
