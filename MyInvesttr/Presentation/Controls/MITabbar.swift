//
//  MITabbar.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 6.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class MITabbar: UITabBar {
	
	override func draw(_ rect: CGRect) {
		super.draw(rect)
		
		// Set Transparet
		self.barTintColor = UIColor.clear
		self.backgroundImage = UIImage()
		self.shadowImage = UIImage()
	}
}
