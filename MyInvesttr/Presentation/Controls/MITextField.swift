//
//  MITextField.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 13.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

//@IBDesignable
final class MITextField: UITextField {

	@IBInspectable
	public var fontName: String = ""
	@IBInspectable
	public var fontSize: CGFloat = 0
	
	@IBInspectable
	public var leftImage: UIImage? {
		didSet {
			if let image = self.leftImage {
				let height = self.bounds.size.height
				let padding: CGFloat = 8

				// Calculate right view frame
				let leftViewContainer = UIView(frame: CGRect(x: 0, y: 0, width: height - padding, height: height))
				let leftViewFrame = CGRect(x: padding, y: padding, width: height - padding*2, height: height - padding*2)
				
				// Setup left view
				let imageView = UIImageView(image: image)
				imageView.frame = leftViewFrame
				imageView.contentMode = .scaleAspectFit
				imageView.tintColor = MITheme().color("grayDarkColor")
				self.leftViewMode = .always
				leftViewContainer.addSubview(imageView)
				self.leftView = leftViewContainer
			}
		}
	}
	
	override public func draw(_ rect: CGRect) {
		if self.fontName != "" {
			self.font = MITheme().font(self.fontName)
		}
		if self.fontSize != 0 {
			self.font = UIFont(name: self.font!.fontName, size: self.fontSize)
		}
		super.draw(rect)
	}
}
