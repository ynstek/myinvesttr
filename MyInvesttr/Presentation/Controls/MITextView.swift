//
//  MITextView.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 28.05.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class MITextView: UITextView {

	@IBInspectable
	public var fontName: String = ""
	@IBInspectable
	public var fontSize: CGFloat = 0
	
	override public func draw(_ rect: CGRect) {
		if self.fontName != "" {
			self.font = MITheme().font(self.fontName)
		}
		if self.fontSize != 0 {
			self.font = UIFont(name: self.font!.fontName, size: self.fontSize)
		}
		super.draw(rect)
	}
}
