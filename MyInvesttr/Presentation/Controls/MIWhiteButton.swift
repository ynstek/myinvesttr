//
//  MIWhiteButton.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 6.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class MIWhiteButton: MIButton {
	
	override func initialize() {
		super.initialize()
		self.backgroundColor = UIColor.white
		self.tintColor = UIColor("#42A5F5")
	}
}
