//
//  MarketCategory.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 31.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

enum MarketCategory: String {
	case forex = "Döviz"
	case cryptoCurrency = "Kripto Paralar"
	case gold = "Altın"
	
	func categoryKey() -> String {
		switch self {
		case .forex:
			return "FXList"
		case .cryptoCurrency:
			return "CryptoCurrencyList"
		case .gold:
			return "GoldList"
		}
	}
}
