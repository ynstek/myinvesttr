//
//  MarketDetailCategory.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 2.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

enum MarketDetailCategory: String {
	case datas = "Veriler"
	case articles = "Makaleler"
	case news = "Haberler"
}
