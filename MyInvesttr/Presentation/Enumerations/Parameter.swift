//
//  Parameter.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 31.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

enum Parameter: String {
	case page = "page"
	case size = "size"
	case category = "category"
	case keys = "keys"
	case tags = "tags"
}
