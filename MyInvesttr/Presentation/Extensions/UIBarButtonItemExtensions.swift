//
//  UIBarButtonItemExtensions.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 24.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

extension UIBarButtonItem {
	
	// MARK: Add Button
	convenience init(imgName: String,
					 target: UIViewController? = UIApplication.shared.topMostViewController(),
					 action: Selector,
					 w: Int = 30,
					 h: Int = 22
		){
		let button: UIButton = UIButton(type: .custom)
		let image = UIImage(named: imgName)
		button.setImage(image, for: UIControl.State())
//		button.frame = CGRect(x: 20, y: 20, width: w, height: h)
		button.bounds = CGRect(x: 20, y: 20, width: w, height: h)
		button.tintColor = UIColor.white
		button.addTarget(target, action: action, for: .touchUpInside)
		self.init(customView: button)
	}
}
