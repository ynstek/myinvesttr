//
//  UIImageExtensions.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 6.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation
import Kingfisher

extension UIImage {
	static var placeHolder: UIImage! {
		return UIImage(named: "favicon")
	}
}
