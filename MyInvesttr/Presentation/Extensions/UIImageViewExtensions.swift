//
//  UIImageViewExtensions.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 27.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {

	func downloadImage(_ urlString: String?, makeRound: Bool = false, customOptions: KingfisherOptionsInfo = []) {
		guard urlString != nil, !urlString!.isEmpty, let url = URL(string: urlString!) else { return }
		
		var processor: ImageProcessor!
		if makeRound {
			processor = DownsamplingImageProcessor(size: self.frame.size) >> RoundCornerImageProcessor(cornerRadius: self.frame.size.height)
		}
		
		self.kf.indicatorType = .activity
		
		var options: KingfisherOptionsInfo = [
			.scaleFactor(UIScreen.main.scale),
			.transition(.fade(0)),
			.cacheOriginalImage
		]
		
		if processor != nil {
			options.append(
				.processor(processor)
			)
		}
		
		for co in customOptions {
			options.append(co)
		}
		
		self.kf.setImage(
			with: url,
			placeholder: UIImage.placeHolder,
			options: options
			)
		{
			result in
			switch result {
			case .success(let value):
				print("Download Image Task done for: \(value.source.url?.absoluteString ?? "")")
			case .failure(let error):
				print("Download Image Job failed: \(error.localizedDescription)")
			}
		}
	}
}
