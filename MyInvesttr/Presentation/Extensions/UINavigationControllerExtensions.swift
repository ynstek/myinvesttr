//
//  UIViewController+NavigationBar.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 6.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

extension UINavigationController {
	
	open override var preferredStatusBarStyle: UIStatusBarStyle {
		return topViewController?.preferredStatusBarStyle ?? .lightContent
	}
	
	public func prepareNavBar() {
		if self.navigationBar.topItem?.title == nil {
			self.navigationBar.topItem?.title = " " // x10 character
		}

		let color = self.theme.color("white")
		self.navigationBar.tintColor = color
		// for default navigation bar title color
		self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
	}
	
//	public func presentTransparentNavigationBar(isHidden: Bool = true) {
//		guard let navController = self.navigationController else { return }
//		navController.navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
//		navController.navigationBar.isTranslucent = isHidden
//		navController.navigationBar.shadowImage = UIImage()
//		navController.setNavigationBarHidden(false, animated:true)
//		navController.navigationItem.hidesBackButton = isHidden
//		//        self.edgesForExtendedLayout = UIRectEdge.None // Kenarlar
//		navController.navigationBar.barTintColor = UIColor.white
//		navController.navigationBar.tintColor = UIColor.white
//	}
}
