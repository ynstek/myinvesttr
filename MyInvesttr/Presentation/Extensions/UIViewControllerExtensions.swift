//
//  UIViewControllerExtensions.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 27.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

extension UIViewController: MIThemeable, MIContigurable {
	
	var theme: MITheme {
		return MITheme()
	}
	
	var configuration: MIConfiguration {
		return MIConfiguration()
	}
	
	public func topBarHeight() -> CGFloat {
		return self.getNavigationBarHeight() + self.statusBarHeight()
	}
	
	// MARK: Navbar Height
	
	public func getNavigationBarHeight() -> CGFloat {
		return (self.navigationController?.navigationBar.frame.height ?? 0)
	}
	
	public func statusBarHeight() -> CGFloat {
		return UIApplication.shared.statusBarFrame.height
	}
	
	// MARK: Apply NavigationBar Background
	
	public func applyNavigationBarBackground(color: UIColor) {
		if self.navigationItem.isKind(of: MINavigationItem.self) {
			
			if let navigationBackgroundView = self.view.viewWithTag(MICommonConstants.navigationBackgroundViewTag) {
				navigationBackgroundView.removeFromSuperview()
			}

			// Create view
			let navigationBackgroundView = UIView(frame: CGRect.zero)
			navigationBackgroundView.tag = MICommonConstants.navigationBackgroundViewTag
			navigationBackgroundView.backgroundColor = color
			
			// Set frame
			navigationBackgroundView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.topBarHeight())
			navigationBackgroundView.layer.zPosition = 1000
			
			// Add subview
			self.view.addSubview(navigationBackgroundView)
		}
	}
}
