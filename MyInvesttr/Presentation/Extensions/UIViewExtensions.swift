//
//  UIImageViewExtensions.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 6.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

//@IBDesignable
extension UIView {
	@IBInspectable
	var borderColor: UIColor? {
		set {
			layer.borderColor = newValue?.cgColor
		}
		get {
			guard let color = layer.borderColor else {
				return nil
			}
			return UIColor(cgColor: color)
		}
	}
	
	@IBInspectable
	var borderWidth: CGFloat {
		set {
			layer.borderWidth = newValue
		}
		get {
			return layer.borderWidth
		}
	}
	
	@IBInspectable
	var cornerRadius: CGFloat {
		set {
			layer.cornerRadius = newValue
			clipsToBounds = newValue > 0
		}
		get {
			return layer.cornerRadius
		}
	}
	
	@IBInspectable
	var isRound: Bool {
		set {
			self.layer.cornerRadius = self.frame.size.width / 2
			self.clipsToBounds = true
		}
		get {
			return self.layer.cornerRadius == self.frame.size.width / 2
		}
	}
}

// MARK: Gradient

extension UIView {
	func setGradientBackground(one: UIColor, two: UIColor, pointPozition: GradientColorPozition, alpha: Float) {
		let gradientLayer = CAGradientLayer()
		gradientLayer.frame = self.bounds
		gradientLayer.colors = [one.cgColor, two.cgColor]
		gradientLayer.locations = [0.0, 1.0]
		gradientLayer.opacity = alpha

		switch pointPozition {
		case .topToBottom:
			gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
			gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
			break
		case .letfToRight:
			gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
			gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
			break
		case .bottomToTop:
			gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
			gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
		case .rightToLeft:
			gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.5)
			gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.5)
		}
		
		// 0,0   .5,0   1,0
		// 0,.5  .5,.5  1,.5
		// 0,1   .5,1   1,1

		for layer in layer.sublayers ?? [] {
			if layer.isKind(of: gradientLayer.classForCoder) {
				layer.removeFromSuperlayer()
			}
		}
		
		layer.insertSublayer(gradientLayer, at: 0)
	}
}
