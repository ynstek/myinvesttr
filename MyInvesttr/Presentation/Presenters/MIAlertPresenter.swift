//
//  MIAlertPresenter.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 6.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final public class MIAlertPresenter: NSObject {
    func topController() -> UIViewController {
        var topController = UIApplication.shared.keyWindow!.rootViewController! as UIViewController
        while ((topController.presentedViewController) != nil) {
            topController = topController.presentedViewController!;
        }
        return topController
    }
    
    func alert(_ titleMessage: String?, bodyMessage: String?, yes: @escaping () -> Void, no: @escaping () -> Void){
        let alertController = UIAlertController(title: titleMessage, message: bodyMessage, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Hayır", style: .cancel , handler: {(action: UIAlertAction!) in
            no()
        }))
        
        alertController.addAction(UIAlertAction(title: "Evet", style: .default , handler: { (action: UIAlertAction!) in
            yes()
        }))
        
        self.topController().present(alertController, animated:true, completion:nil)
    }
    
    func alert(_ titleMessage: String, bodyMessage: String, showButton: Bool = true) {
        let alertController = UIAlertController(title: titleMessage, message: bodyMessage, preferredStyle: .alert)

        if showButton {
            let defaultAction = UIAlertAction(title: "Tamam", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.topController().present(alertController, animated:true, completion:nil)
        } else {
            self.topController().present(alertController, animated:true, completion:nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alertController.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func alert(_ titleMessage: String, bodyMessage: String, _ ok: @escaping () -> Void){
        let alertController = UIAlertController(title: titleMessage, message: bodyMessage, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Tamam", style: .default , handler: { (action: UIAlertAction!) in
            ok()
        }))
        
        self.topController().present(alertController, animated:true, completion:nil)
    }
    
    // MARK: Custom Alerts
    
    func alert(_ titleMessage: String, bodyMessage: String, _ switchTitle: String, _ switchStatus: Bool, success: @escaping (_ status: Bool) -> Void) {
        // private Alert
        let alert = UIAlertController(title: titleMessage, message: bodyMessage + "\n\n", preferredStyle: .alert)
        
        // Create Label
        let labelFrame: CGRect = CGRect(x: 0, y: 75, width: 150, height: 20)
        let label: UILabel! = UILabel(frame: labelFrame)
        label.text = switchTitle
        label.textAlignment = .right
        alert.view.addSubview(label)
        
        // Create Switch Object
        let switchFrame: CGRect = CGRect(x: 160, y: 70, width: 20, height: 20)
        let switchObject: UISwitch! = UISwitch(frame: switchFrame)
        switchObject.isOn = switchStatus
        
        alert.view.addSubview(switchObject)
        
        alert.addAction(UIAlertAction(title: "Hayır", style: .cancel , handler: {(alert) in
        }))
        
        alert.addAction(UIAlertAction(title: "Evet", style: .default , handler: { (alert) in
            success(switchObject.isOn)
        }))
        
        self.topController().present(alert, animated: true, completion: nil)
    }
    
    func alert(title: String, pickerdate: Date, minimumdate: Date?, apply: @escaping (_ date: Date) -> Void, cancel: @escaping () -> Void) {
        
        let alert = UIAlertController(title: title, message: "\n\n\n\n\n\n\n\n", preferredStyle: .alert)
        alert.view.subviews.first!.backgroundColor = UIColor.white
        
        // Clock cover: View
        let viewFrame: CGRect = CGRect(x: 198, y: 65, width: 52, height: 150)
        let view: UIView = UIView(frame: viewFrame)
        view.backgroundColor = UIColor.colorWithRedValue(248, 248, 248, alpha: 1)
        alert.view.subviews.first!.layer.cornerRadius = 8
        alert.view.subviews.first!.layer.masksToBounds = true
        
        // Date Picker
        let pickerFrame: CGRect = CGRect(x: 70, y: 55, width: 150, height: 160)
        let picker: UIDatePicker = UIDatePicker(frame: pickerFrame)
        if minimumdate != nil {
            picker.minimumDate = minimumdate
        }
        picker.locale = Locale(identifier: "tr_TR")
        picker.date = pickerdate
        
        alert.view.addSubview(picker)
        alert.view.addSubview(view)
        
        alert.addAction(UIAlertAction(title: "İptal", style: .cancel , handler: {(alert) in
            cancel()
        }))
        
        alert.addAction(UIAlertAction(title: "Uygula", style: .default , handler: { (alert) in
            apply(picker.date)
        }))
        
        self.topController().present(alert, animated: true, completion: nil)
    }
}
