//
//  MIIndicatorPresenter.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 6.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import Lottie

final public class MIIndicatorPresenter: NSObject {
	
	// MARK: - Constants
	
	private let animationViewJson = "MIActivityIndicator"
	private let animationViewTag = 1000
	private let transformScale: CGFloat = 0.2
	private let animationSpeed: CGFloat = 2
	
	// MARK: Properties
	
	fileprivate var animationView: AnimationView?
	fileprivate var isHidden = false
	
	// MARK: Presenter Methods
	
	func present() {
		presentInternal(in: nil)
	}
	
	func hide() {
		hideInternal(in: nil)
	}
	
	func hide(in viewController: UIViewController?) {
		hideInternal(in: viewController)
	}
	
	// MARK: Helper Methods
	
	private func hideInternal(in viewController: UIViewController?) {
		
		// Close activity
		UIActivityIndicatorView().stopAnimating()
		UIApplication.shared.isNetworkActivityIndicatorVisible = false
		
		// Configure animation view
		var animationViewController = viewController
		if animationViewController == nil {
			animationViewController = UIApplication.shared.topMostViewController()
		}
		
		// Pause animation
		self.animationView?.pause()
		
		// Remove animation view
		if let animationView = animationViewController?.view.viewWithTag(self.animationViewTag) {
			animationView.removeFromSuperview()
			
			// Set visibility
			self.isHidden = false
		}
		
		// End ignore interaction event
		if UIApplication.shared.isIgnoringInteractionEvents {
			UIApplication.shared.endIgnoringInteractionEvents()
		}
	}
	
	private func presentInternal(in viewController: UIViewController?) {
		
		self.hideInternal(in: viewController)
		
		// Open activity
		UIActivityIndicatorView().startAnimating()
		UIApplication.shared.isNetworkActivityIndicatorVisible = true

		// Start ignore interaction event
		if !UIApplication.shared.isIgnoringInteractionEvents {
			UIApplication.shared.beginIgnoringInteractionEvents()
		}

		if self.isHidden {
			return
		}

		// Configure animation view
		var animationViewController = viewController
		if animationViewController == nil {
			animationViewController = UIApplication.shared.topMostViewController()
		}

		self.animationView = AnimationView(name: self.animationViewJson)
		self.animationView?.contentMode = .scaleAspectFit
		self.animationView?.tag = self.animationViewTag
		self.animationView?.loopMode = .loop
		self.animationView?.frame = animationViewController?.view.bounds ?? .zero
		self.animationView?.transform = CGAffineTransform(scaleX: self.transformScale, y: self.transformScale)
		self.animationView?.play()
		self.animationView?.animationSpeed = animationSpeed

		// Set visibility
		self.isHidden = true

		// Add subview
		animationViewController?.view.addSubview(self.animationView!)
		animationViewController?.view.bringSubviewToFront(self.animationView!)
	}
}
