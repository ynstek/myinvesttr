//
//  MIMenuViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 27.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

open class MIMenuViewController: MIViewController, SWRevealViewControllerDelegate {

	// MARK: Variables
	
	public var menuSegueIdentifier: MenuSegueIdentifier!
	private var closeMenuView = UIView()
	private let closeMenuViewTag = 1000
	
	override open func viewDidLoad() {
		super.viewDidLoad()
		self.prepareUI()
		self.revealViewController().delegate = self
	}
	
	open override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationItem.leftBarButtonItem = UIBarButtonItem(imgName: "menu", target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
	}
	
	// MARK: Prepare UI
	
	fileprivate func prepareUI() {
		// Prepare CloseMenuView
		self.closeMenuView.frame = UIScreen.main.bounds
		self.closeMenuView.backgroundColor = UIColor.clear
		self.closeMenuView.tag = self.closeMenuViewTag
		self.closeMenuView.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
		self.closeMenuView.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
	}
	
	public func revealController(_ revealController: SWRevealViewController!, animateTo position: FrontViewPosition) {
		if position == .right {
			if let vc = (self.topMostViewController() as? SWRevealViewController)?.frontViewController {
				vc.view.addSubview(self.closeMenuView)
				vc.view.bringSubviewToFront(self.closeMenuView)
			}
		} else {
			if let vc = (self.topMostViewController() as? SWRevealViewController)?.frontViewController {
				// Remove closeMenu view
				if let view = vc.view.viewWithTag(self.closeMenuViewTag) {
					view.removeFromSuperview()
				}
			}
		}
	}

}
