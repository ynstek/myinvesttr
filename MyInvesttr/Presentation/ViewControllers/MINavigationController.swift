//
//  MINavigationController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 16.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class MINavigationController: UINavigationController {
	
	// MARK: Set For Statusbar Style
	
	public override var childForStatusBarStyle: UIViewController? {
		return visibleViewController
	}
}
