//
//  ButtonBarPagerTabStripViewController+Helper.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 12.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import XLPagerTabStrip

open class MIPagerViewController: ButtonBarPagerTabStripViewController, SWRevealViewControllerDelegate {
	
	// MARK: Public Variables

	public var haveMenuButton = true
	public var navigationBackgroundColor: UIColor = MITheme().color("greenDarkColor")
	public var barbuttonBackgroundColor = MITheme().color("greenDark")
	
	// MARK: Private Variables
	
	private var closeMenuView = UIView()
	private let closeMenuViewTag = 1000
	
	override open func viewDidLoad() {
		self.prepareUI()
		// Super ViewDidLoad
		super.viewDidLoad()
		self.revealViewController().delegate = self
		self.applyNavigationBarBackground(color: navigationBackgroundColor)
	}
	
	override open func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if self.haveMenuButton {
			self.navigationItem.leftBarButtonItem = UIBarButtonItem(imgName: "menu", target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
		}
	}
	
	// MARK: Prepare UI
	
	fileprivate func prepareUI() {
		self.preparePager()
		
		// Prepare CloseMenuView
		self.closeMenuView.frame = UIScreen.main.bounds
		self.closeMenuView.backgroundColor = UIColor.clear
		self.closeMenuView.tag = self.closeMenuViewTag
		self.closeMenuView.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
		self.closeMenuView.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
	}
	
	// MARK: Set For Statusbar Style
	
	open override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	// MARK: Safe Area
	
	open override func viewSafeAreaInsetsDidChange() {
		if #available(iOS 11.0, *) {
			super.viewSafeAreaInsetsDidChange()
			
			if let navigationBackgroundView = self.view.viewWithTag(MICommonConstants.navigationBackgroundViewTag) {
				let safeAreaFrame = self.view.safeAreaLayoutGuide.layoutFrame
				navigationBackgroundView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: safeAreaFrame.origin.y)
			}
		}
	}
	
	open override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		if #available(iOS 11.0, *) {
		} else {
			if let navigationBackgroundView = self.view.viewWithTag(MICommonConstants.navigationBackgroundViewTag) {
				navigationBackgroundView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.topLayoutGuide.length)
			}
		}
	}
	
	// MARK: Menu
	
	public func revealController(_ revealController: SWRevealViewController!, animateTo position: FrontViewPosition) {
		if position == .right {
			if let vc = (self.topMostViewController() as? SWRevealViewController)?.frontViewController {
				vc.view.addSubview(self.closeMenuView)
				vc.view.bringSubviewToFront(self.closeMenuView)
			}
		} else {
			if let vc = (self.topMostViewController() as? SWRevealViewController)?.frontViewController {
				// Remove closeMenu view
				if let view = vc.view.viewWithTag(self.closeMenuViewTag) {
					view.removeFromSuperview()
				}
			}
		}
	}

	func preparePager() {
		// Selected bar color
		let titleColor = self.theme.color("white")
		self.settings.style.buttonBarBackgroundColor = self.barbuttonBackgroundColor
		self.settings.style.buttonBarItemBackgroundColor = UIColor.clear
		self.settings.style.selectedBarBackgroundColor = titleColor
		self.settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
		self.settings.style.selectedBarHeight = 2.0
		self.settings.style.buttonBarMinimumLineSpacing = 0
		self.settings.style.buttonBarItemTitleColor = titleColor
		self.settings.style.buttonBarItemsShouldFillAvailableWidth = true
		self.settings.style.buttonBarLeftContentInset = 8
		self.settings.style.buttonBarRightContentInset = 8
		
		// Selected bar Animation
		self.changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
			guard changeCurrentIndex == true else { return }
			
			if animated {
				UIView.animate(withDuration: 0.1, animations: {
					newCell?.transform = CGAffineTransform(scaleX: 1.06, y: 1.06)
					oldCell?.transform = CGAffineTransform(scaleX: 1, y: 1)
				})
			} else {
				newCell?.transform = CGAffineTransform(scaleX: 1.06, y: 1.06)
				oldCell?.transform = CGAffineTransform(scaleX: 1, y: 1)
			}
			
			oldCell?.label.textColor = self.theme.color("grayDark")
			newCell?.label.textColor = self.theme.color("white")
		}
	}

	// MARK: - Create ViewController Methods
	
	func getViewController(storyboardName: StoryboardNameConstants) -> UIViewController? {
		let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
		let controller = storyboard.instantiateInitialViewController()
		return controller
	}
	
	func getViewController(storyboardName: StoryboardNameConstants, viewControllerId: ViewIdConstants) -> UIViewController? {
		let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
		let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId.rawValue)
		return controller
	}
	
	// MARK: - Navigation Methods
	
	// MARK: Push
	
	func push(_ viewController: UIViewController, animated: Bool = true) {
		self.navigationController?.prepareNavBar()
		self.navigationController?.pushViewController(viewController, animated: animated)
	}
	
	func push(storyboardName: StoryboardNameConstants, animated: Bool = true) {
		let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
		let controller = storyboard.instantiateInitialViewController()
		self.push(controller!, animated: animated)
	}
	
	func push(storyboardName: StoryboardNameConstants, viewControllerId: ViewIdConstants, animated: Bool = true) {
		let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
		let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId.rawValue)
		self.push(controller, animated: animated)
	}
	
	// MARK: Present
	
	override open func present(_ viewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
		self.navigationController?.prepareNavBar()
		super.present(viewController, animated: animated, completion: completion)
	}
	
	func present(storyboardName: StoryboardNameConstants, animated: Bool = true, completion: (() -> Void)? = nil) {
		let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
		let controller = storyboard.instantiateInitialViewController()
		self.present(controller!, animated: animated, completion: completion)
	}
	
	func present(storyboardName: StoryboardNameConstants, viewControllerId: ViewIdConstants, animated: Bool = true, completion: (() -> Void)? = nil) {
		let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
		let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId.rawValue)
		self.present(controller, animated: animated, completion: completion)
	}
}
