//
//  MIViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 6.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SnapKit

open class MIViewController: UIViewController {
	
	// MARK: Properties
	
	public var itemInfo: IndicatorInfo = ""
	public var categoryKey: String = ""
	public var tagKey: String = ""
	public var selectedPageNo: Int = 1
	public var isExistsList: Bool = true
	public var navigationBackgroundColor: UIColor = MITheme().color("greenDarkColor")
	
	open override func viewDidLoad() {
		// Super ViewDidLoad
		super.viewDidLoad()
		self.applyNavigationBarBackground(color: self.navigationBackgroundColor)
		self.navigationController?.prepareNavBar()
	}
	
	// MARK: Safe Area
	
	open override func viewSafeAreaInsetsDidChange() {
		if #available(iOS 11.0, *) {
			super.viewSafeAreaInsetsDidChange()
			
			// NavigationBackgroundView
			
			if let navigationBackgroundView = self.view.viewWithTag(MICommonConstants.navigationBackgroundViewTag) {
				let safeAreaFrame = self.view.safeAreaLayoutGuide.layoutFrame
				navigationBackgroundView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: safeAreaFrame.origin.y)
			}
		}
	}
	
	open override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		// NavigationBackgroundView
		
		if #available(iOS 11.0, *) {
		} else {
			if let navigationBackgroundView = self.view.viewWithTag(MICommonConstants.navigationBackgroundViewTag) {
				navigationBackgroundView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.topLayoutGuide.length)
			}
		}
	}
	
	// MARK: Set Statusbar color
	
	open override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	// MARK: Helper Methods
	
	func registerTableViewNibs(_ identifier: ViewIdConstants, tableView: UITableView) {
		let nib = UINib(nibName: identifier.rawValue, bundle: nil)
		tableView.register(nib, forCellReuseIdentifier: identifier.rawValue)
	}
}

// MARK: - Navigation Methods

extension MIViewController {
	
	// MARK: - Create ViewController Methods
	
	func getView(nibName: String) -> UIView {
		return Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?.first as! UIView
	}
	
	func getViewController(storyboardName: StoryboardNameConstants) -> UIViewController? {
		let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
		let controller = storyboard.instantiateInitialViewController()
		return controller
	}
	
	func getViewController(storyboardName: StoryboardNameConstants, viewControllerId: ViewIdConstants) -> UIViewController? {
		let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
		let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId.rawValue)
		return controller
	}
	
	// MARK: Push
	
	func push(_ viewController: UIViewController, animated: Bool = true) {
		self.navigationController?.prepareNavBar()
		self.navigationController?.pushViewController(viewController, animated: animated)
	}
	
	func push(storyboardName: StoryboardNameConstants, animated: Bool = true) {
		let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
		let controller = storyboard.instantiateInitialViewController()
		self.push(controller!, animated: animated)
	}
	
	func push(storyboardName: StoryboardNameConstants, viewControllerId: ViewIdConstants, animated: Bool = true) {
		let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
		let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId.rawValue)
		self.push(controller, animated: animated)
	}
	
	// MARK: Present
	
	override open func present(_ viewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
		self.navigationController?.prepareNavBar()
		super.present(viewController, animated: animated, completion: completion)
	}
	
	func present(storyboardName: StoryboardNameConstants, animated: Bool = true, completion: (() -> Void)? = nil) {
		let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
		let controller = storyboard.instantiateInitialViewController()
		self.present(controller!, animated: animated, completion: completion)
	}
	
	func present(storyboardName: StoryboardNameConstants, viewControllerId: ViewIdConstants, animated: Bool = true, completion: (() -> Void)? = nil) {
		let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
		let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId.rawValue)
		self.present(controller, animated: animated, completion: completion)
	}
	
	// MARK: Segue
	
	open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		super.prepare(for: segue, sender: sender)
		self.navigationController?.prepareNavBar()
	}
}

// MARK: - IndicatorInfoProvider

extension MIViewController: IndicatorInfoProvider {
	public func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
		return itemInfo
	}
}
