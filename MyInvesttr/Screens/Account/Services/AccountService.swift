//
//  AccountService.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 24.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final public class AccountService: MIService {
	
	// MARK: Properties
	
	private let loginPath = MIConfiguration().configForKey("LOGIN_PATH") as! String
	private let registerPath = MIConfiguration().configForKey("REGISTER_PATH") as! String
	private let forgotPasswordPath = MIConfiguration().configForKey("FORGOT_PASSWORD_PATH") as! String
	private let staticContentPath = MIConfiguration().urlForKeyWithVersion("STATIC_CONTENT_PATH")

	// MARK: Call Services
	
	public func fetch(requestModel: LoginRequestModel, _ successClosure: @escaping SuccessClosure<LoginResponseModel>, errorClosure: ErrorClosure? = nil) {
	
		// Call Service
		self.call(requestModel, suffixPath: self.loginPath, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	public func fetch(requestModel: RegisterRequestModel, _ successClosure: @escaping SuccessClosure<RegisterResponseModel>, errorClosure: ErrorClosure? = nil) {
	
		// Call Service
		self.call(requestModel, suffixPath: self.registerPath, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	public func fetch(requestModel: ForgotPasswordRequestModel, _ successClosure: @escaping SuccessClosure<ForgotPasswordResponseModel>, errorClosure: ErrorClosure? = nil) {
		
		// Call Service
		self.call(requestModel, suffixPath: self.forgotPasswordPath, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	public func fetch(key: String, _ successClosure: @escaping SuccessClosure<StaticContentModel>, errorClosure: ErrorClosure? = nil) {

		// Parameters
		var queryItems = [URLQueryItem]()
		queryItems.append(URLQueryItem(name: Parameter.keys.rawValue, value: key))
		
		// Call Service
		self.call(queryItems, suffixPath: self.staticContentPath, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	// MARK: Indicator Strategy
	
	public func indicatorStrategy(_ status: IndicatorStrategyResult) -> AccountService {
		self.indicatorStrategy = status
		return self
	}
}
