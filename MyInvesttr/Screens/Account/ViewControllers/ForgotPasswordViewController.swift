//
//  ForgotPasswordViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 20.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class ForgotPasswordViewController: MIViewController {

	// MARK: Outlets
	
	@IBOutlet var emailTextField: MITextField!
	
	// MARK: Variables
	
	private let service = AccountService()
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		self.prepareUI()
	}
	
	func prepareUI() {
		self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.closeButton))
		
		self.title = "Şifreni Sıfırla"
	}
	
	@objc func closeButton() {
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func confirmButton(_ sender: Any) {
		if let email = emailTextField.text, email.isValidEmail() {
			let model = ForgotPasswordRequestModel()
			model.email = email
			self.service
				.fetch(requestModel: model, { response in
					if response.status == true {
						self.dismiss(animated: true, completion: {
							self.showAlert("İşlem Başarılı", bodyMessage: "Şifrenizi yenilenemeniz için size email gönderdik.")
						})
					} else {
						self.showAlert("UYARI!", bodyMessage: response.errorMessage ?? "Lütfen bilgilerinizi ve kontrol edip tekrar deneyiniz.")
					}
				})
		}
	}
}
