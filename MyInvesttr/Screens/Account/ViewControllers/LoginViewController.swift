//
//  LoginViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 20.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class LoginViewController: MIViewController {

	// MARK: Outlets
	
	@IBOutlet var emailTextField: MITextField!
	@IBOutlet var passwordTextField: MITextField!
	
	// MARK: Variables
	
	private let service = AccountService()
	
	override func viewDidLoad() {
        super.viewDidLoad()
		self.prepareUI()
	}
	
	func prepareUI() {
		self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.closeButton))

		self.title = "Giriş Yap"
	}

	@objc func closeButton() {
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func loginButton(_ sender: Any) {
		if self.validate() {
			let loginModel = LoginRequestModel()
			loginModel.email = self.emailTextField.text
			loginModel.password = self.passwordTextField.text
			
			self.service
				.fetch(requestModel: loginModel, { response in					
					if response.status == true {
						UserDefaults.standard.set(try? PropertyListEncoder().encode(response), forKey: "LoginResponseModel")
						MIFirebase().firebaseSetTokenAndTopic()
						self.closeButton()
					} else {
						self.showAlert("UYARI!", bodyMessage: "Lütfen Email ve Şifre alanlarını kontrol edip tekrar deneyiniz.")
					}
				})
		}
	}
	
	func validate() -> Bool {
		guard let email = self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
			let password = self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
			else { return false }
		
		if email.isEmpty || password.isEmpty {
			self.showAlert("Email veya Şifre alanı boş!", bodyMessage: "Lütfen Email ve Şifre alanlarını boş geçmeyiniz.")
			return false
		} else if !email.isValidEmail() {
			return false
		}
		
		// Cleared whitespaces
		self.emailTextField.text = email
		self.passwordTextField.text = password
		return true
	}
	
	@IBAction func registerButton(_ sender: Any) {
	}
	
	@IBAction func forgotPassword(_ sender: Any) {
	}
}
