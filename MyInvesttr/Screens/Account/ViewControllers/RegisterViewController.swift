//
//  RegisterViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 20.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import Atributika

final class RegisterViewController: MIViewController {

	// MARK: Outlets
	
	@IBOutlet var confirmAttributedLabel: AttributedLabel!
	@IBOutlet var firstnameTextField: MITextField!
	@IBOutlet var lastnameTextField: MITextField!
	@IBOutlet var phoneTextField: MITextField!
	@IBOutlet var emailTextField: MITextField!
	@IBOutlet var passwordTextField: MITextField!
	
	// MARK: Variables
	
	private let service = AccountService()
	
	override func viewDidLoad() {
        super.viewDidLoad()
		self.prepareUI()
    }
	
	func prepareUI() {
		self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.closeButton))

		self.title = "Kayıt ol"
		self.prepareConfirmLabel()
	}
	
	@objc func closeButton() {
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func confirmButton(_ sender: Any) {
		if let model = self.validate() {
			self.service
				.fetch(requestModel: model, { response in
					if response.status == true {
						self.showAlert("İşlem Başarılı", bodyMessage: "Üyelik başvurunuz alındı! E-postanınız kontrol ediniz.", ok: {
							self.dismiss(animated: true, completion: nil)
						})
						
					} else {
						self.showAlert("UYARI!", bodyMessage: response.errorMessage ?? "Lütfen bilgilerinizi kontrol ederek tekrar deneyiniz.")
					}
				})
		}
	}
	
	func validate() -> RegisterRequestModel? {
		guard let email = self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
			let password = self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
			let firstname = self.firstnameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
			let lastname = self.lastnameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
			let phone = self.phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
			else { return nil }
		
		if firstname.isEmpty, lastname.isEmpty, phone.isEmpty, email.isEmpty || password.isEmpty {
			self.showAlert("Email veya Şifre alanı boş!", bodyMessage: "Lütfen Email ve Şifre alanlarını boş geçmeyiniz.")
			return nil
		} else if firstname.count < 3 || lastname.count < 3 {
			self.showAlert("Ad veya Soyad alanı doğru değil!", bodyMessage: "Lütfen Ad ve Soyad alanlarını kontrol edip tekrar deneyiniz.")
			return nil
		} else if !phone.isValidPhone() {
			return nil
		} else if !email.isValidEmail() {
			return nil
		}
		
		let registerModel = RegisterRequestModel()
		registerModel.email = email
		registerModel.password = password
		registerModel.firstName = firstname
		registerModel.lastName = lastname
		registerModel.phone = phone
		
		return registerModel
	}
	
	fileprivate func prepareConfirmLabel() {
		// Set number of lines
		self.confirmAttributedLabel.numberOfLines = 0
		
		// Set alignment
		self.confirmAttributedLabel.textAlignment = .center
		
		// Configure all text font
		let styleAll = Style.font(self.theme.font( "fontRegular")).foregroundColor(self.theme.color("black"))
		
		// Configure strong style
		let strongTag = Style("strong").font(self.theme.font("fontBold"))
		
		// Configure href style
		let hrefTag = Style("a")
			.foregroundColor(UIColor.blue, .highlighted)
			.foregroundColor(UIColor.red)
		
		// Set text
		self.confirmAttributedLabel.attributedText = "Kayıt olarak <a href=\"uyelikSozlesmesi\">Üyelik Sözleşmesi</a> ve <a href=\"gizlilikPolitikasi\">Gizlilik Politikasını</a> kabul etmiş olursunuz."
			.style(tags: [strongTag, hrefTag])
			.styleAll(styleAll)
		
		self.confirmAttributedLabel.onClick = { [weak self] _, detection in
			guard let self = self else { return }
			switch detection.type {
			case .tag(let tag):
				if tag.name == "a", let href = tag.attributes["href"] {
					let webViewC = self.getViewController(storyboardName: .WebView) as! WebViewViewController

					if href == "uyelikSozlesmesi" {
						webViewC.title = "Üyelik Sözleşmesi"
						webViewC.htmlContentText = ""

						self.service.fetch(key: "termsandconditions", { response in
							webViewC.htmlContentText = response.text ?? ""
							self.push(webViewC)
						})

					} else if href == "gizlilikPolitikasi" {
						webViewC.title = "Gizlilik Politikası"
						webViewC.htmlContentText = ""
						
						self.service.fetch(key: "privacypolicy", { response in
							webViewC.htmlContentText = response.text ?? ""
							self.push(webViewC)
						})
					}
				}
			default:
				break
			}
		}
	}
}
