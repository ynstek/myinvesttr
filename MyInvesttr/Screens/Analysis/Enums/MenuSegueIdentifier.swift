//
//  MenuSegueIdentifier.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 27.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

public enum MenuSegueIdentifier: String {
	case contact = "Contact"
	case home = "Home"
	case technicalAnalysis = "TechnicalAnalysis"
	case videoAnalysis = "VideoAnalysis"
	case forexSignals = "ForexSignals"
	case economicCalendar = "EconomicCalendar"
	case forexConverter = "ForexConverter"
	case signOut
}
