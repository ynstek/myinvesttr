//
//  AnalysisService.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 25.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final public class AnalysisService: MIService {

	// MARK: Properties
	
	private let technicalAnalysisPath = MIConfiguration().urlForKeyWithVersion("TECHNICAL_ANALYSIS_PATH")
	private let videoAnalysisPath = MIConfiguration().urlForKeyWithVersion("VIDEO_ANALYSIS_PATH")

	// MARK: Call Services
	
	public func fetch(identifier: MenuSegueIdentifier, pageNo: Int, _ successClosure: @escaping SuccessClosure<[ListModel]>, errorClosure: ErrorClosure? = nil) {
		
		// Parameters
		var queryItems = [URLQueryItem]()
		queryItems.append(URLQueryItem(name: Parameter.page.rawValue, value: String(pageNo)))
		queryItems.append(URLQueryItem(name: Parameter.size.rawValue, value: String(50)))
		
		var path = ""
		
		switch identifier {
		case .technicalAnalysis:
			path = technicalAnalysisPath
		case .videoAnalysis:
			path = videoAnalysisPath
		default:
			break
		}
		
		// Call Service
		self.call(queryItems, suffixPath: path, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	// MARK: Indicator Strategy
	
	public func indicatorStrategy(_ status: IndicatorStrategyResult) -> AnalysisService {
		self.indicatorStrategy = status
		return self
	}
}
