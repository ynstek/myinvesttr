//
//  AnalysisViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 25.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import AVKit
import XCDYouTubeKit

final class AnalysisViewController: MIMenuViewController {

	// MARK: Outlers
	
	@IBOutlet var tableView: UITableView!
	
	// MARK: Variables
	
	private let service = AnalysisService()
	private var viewModel: AnalysisViewModel?
	private var selectedModel: AnalysisViewModel!
	private let cellIdentifier = ViewIdConstants.AnalysisTableViewCell

	override func viewDidLoad() {
        super.viewDidLoad()
		
		// PrepareUI
		self.prepareUI()
		
		// Get List
		self.getList()
    }
	
	// MARK: Prepare UI
	
	func prepareUI() {
		// Prepare TableView
		self.tableView.delegate = self
		self.tableView.dataSource = self
		self.tableView.rowHeight = UITableView.automaticDimension
		self.tableView.estimatedRowHeight = 76
		self.registerTableViewNibs(self.cellIdentifier, tableView: self.tableView)
	}
	
	// MARK: Call Service
	
	func getList(isFirstPage: Bool = true) {
		if isFirstPage {
			self.selectedPageNo = 1
			self.isExistsList = true
			
			self.service
				.fetch(identifier: self.menuSegueIdentifier, pageNo: self.selectedPageNo, { [weak self] response in
					guard let self = self else {return}
					if response.count == 0 {
						self.isExistsList = false
					}
					self.viewModel = AnalysisViewModel(response: response)
					self.tableView.reloadData()
				})
		} else if isExistsList {
			self.selectedPageNo += 1
			
			self.service
				.indicatorStrategy(.hide)
				.fetch(identifier: self.menuSegueIdentifier, pageNo: self.selectedPageNo, { [weak self] response in
					guard let self = self else {return}
					if response.count == 0 {
						self.isExistsList = false
					}
					DispatchQueue.main.async() {
						self.viewModel!.list += AnalysisViewModel(response: response).list
						self.tableView.reloadData()
					}
				})
		}
	}
	
	// MARK: Open Video
	
	private func openVideo(urlString: String) {
		struct YouTubeVideoQuality {
			static let hd720 = NSNumber(value: XCDYouTubeVideoQuality.HD720.rawValue)
			static let medium360 = NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)
			static let small240 = NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)
		}
		
		let videoURL = URL(string: urlString)
		if videoURL?.scheme == nil, let videoIdentifier = videoURL?.absoluteString {
			// Youtube video
			let playerViewController = AVPlayerViewController()
			present(playerViewController, animated: true)
			
			weak var weakPlayerViewController: AVPlayerViewController? = playerViewController
			XCDYouTubeClient.default().getVideoWithIdentifier(videoIdentifier, completionHandler: { video, error in
				if video != nil {
					var streamURLs = video?.streamURLs
					let streamURL = streamURLs?[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.HD720.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)]
					if let streamURL = streamURL {
						weakPlayerViewController?.player = AVPlayer(url: streamURL)
					}
					weakPlayerViewController?.player?.play()
				} else {
					self.dismiss(animated: true)
				}
			})
		} else if let url = videoURL {
			let player = AVPlayer(url: url)
			let playerViewController = AVPlayerViewController()
			playerViewController.player = player
			present(playerViewController, animated: true) {
				player.play()
			}
		}
		
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		super.prepare(for: segue, sender: sender)
		if segue.identifier == "Detail" {
			if let detailViewController = segue.destination as? AnalysisDetailViewController {
				detailViewController.title = self.selectedModel.propertyTitle
				detailViewController.viewModel = self.selectedModel
			}
		}
	}
}

// MARK: - TableView

extension AnalysisViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.viewModel?.list.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier.rawValue, for: indexPath) as! AnalysisTableViewCell
		let item = self.viewModel!.list[indexPath.row]
		cell.configureModel(item)
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		// Get which cell is selected on the tableview
		self.selectedModel = self.viewModel!.list[indexPath.row];
		
		// Deselect item
		tableView.deselectRow(at: indexPath, animated: true)
		
		// Open PDF or video
		switch self.menuSegueIdentifier {
		case .technicalAnalysis?:
			// Open Detail
			self.performSegue(withIdentifier: "Detail", sender: nil)
			break
		case .videoAnalysis?:
			// Open Detail
			self.openVideo(urlString: self.selectedModel.videoPath)
			break
		default:
			break
		}
	}
}

// MARK: - Scroll

extension AnalysisViewController: UIScrollViewDelegate {
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if scrollView.contentOffset.x == 0 && scrollView.contentOffset.y != 0 {
			if (scrollView.contentOffset.y == (scrollView.contentSize.height - scrollView.frame.size.height)) {
				self.getList(isFirstPage: false)
			}
		}
	}
}
