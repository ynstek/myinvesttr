//
//  AnalysisViewModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 25.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

final public class AnalysisViewModel: MIViewModel {

	// MARK: Properties
	
	public var list: [AnalysisViewModel] = []
	public var propertyTitle: String = ""
	public var date: String = ""
	public var htmlContent: String = ""
	public var videoPath: String = ""
	public var videoThumb: String = ""

	// MARK: - Init Methods
	
	public init(response: [ListModel]) {
		self.list = [AnalysisViewModel]()
		for model in response {
			let analysisViewModel = AnalysisViewModel(response: model)
			self.list.append(analysisViewModel)
		}
		super.init()
	}
	
	public init(response: ListModel) {
		self.propertyTitle = response.properties?.title ?? ""
		self.date = response.publishDate?.toString(format: MICommonConstants.defaultDisplayDateTimeLongFormat) ?? ""
		self.htmlContent = response.properties?.content ?? ""
		self.videoPath = response.properties?.videoPath ?? ""
		self.videoThumb = response.properties?.videoThumb ?? ""
		super.init()
	}
}
