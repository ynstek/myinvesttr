//
//  AnalysisDetailViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 8.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class AnalysisDetailViewController: MIViewController {

	// MARK: - Outlets

	@IBOutlet var titleLabel: UILabel!
	@IBOutlet var content: UITextView!

	// MARK: Properties
	
	public var viewModel: AnalysisViewModel!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		self.prepareUI()
    }

	func prepareUI() {
		self.titleLabel.text = self.viewModel.propertyTitle
		self.content.text = ""
		DispatchQueue.main.async {
			MIIndicatorPresenter().present()
			self.content.attributedText = self.viewModel.htmlContent.html2AttributedString
			MIIndicatorPresenter().hide()
		}
	}
}
