//
//  AnalysisTableViewCell.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 27.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class AnalysisTableViewCell: UITableViewCell {
	
	// MARK: - Outlets
	
	@IBOutlet var titleLabel: UILabel!
	@IBOutlet var date: UILabel!
	@IBOutlet var previewImageView: UIImageView!
	
	public func configureModel(_ model: AnalysisViewModel) {
		self.titleLabel.text = model.propertyTitle
		self.date.text = model.date
		
		if !model.videoThumb.isEmpty {
			self.previewImageView.isHidden = false
			self.previewImageView.downloadImage(model.videoThumb)
		} else {
			self.previewImageView.isHidden = true
		}
	}
}
