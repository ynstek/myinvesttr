//
//  ArticleDetailViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 24.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class ArticleDetailViewController: MIViewController {

	// MARK: - Outlets
	
	@IBOutlet var img: UIImageView!
	@IBOutlet var nameLabel: MILabel!
	@IBOutlet var date: UILabel!
	@IBOutlet var titleLabel: UILabel!
	@IBOutlet var content: UITextView! 
	
	// MARK: Properties
	
	public var model: ListModel!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		self.prepareUI()
    }
	
	func prepareUI() {
		self.nameLabel.text = self.model.author?.authorName ?? ""
		self.titleLabel.text = self.model.properties?.title ?? ""
		self.img.downloadImage(self.model.author?.authorAvatarUrl, makeRound: true)
		
		self.date.text = self.model.publishDate?.toString(format: MICommonConstants.defaultDisplayDateTimeLongFormat)
		
		let htmlContentText = self.model.properties?.content ?? ""
		self.content.text = ""
		DispatchQueue.main.async {
			MIIndicatorPresenter().present()
			self.content.attributedText = htmlContentText.html2AttributedString
			MIIndicatorPresenter().hide()
		}
		
		self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(self.sharetButton))
	}
	
	@objc func sharetButton() {
		let imageUrl = self.configuration.configForKey("SHARE_ARTICLE_PREFIX_URL") as! String
		let url = imageUrl + "/" + (self.model.url ?? "")
		let title = self.model.properties?.title ?? ""
		let text = "\(title)\n\(url)"
		
		// set up activity view controller
		let textToShare = [ text ]
		let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
		activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
		// present the view controller
		self.present(activityViewController, animated: true, completion: nil)
	}
}
