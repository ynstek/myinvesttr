//
//  ArticleService.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 22.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Alamofire
import Foundation

final class ArticleService: MIService {

	// MARK: Properties
	
	var path = MIConfiguration().urlForKeyWithVersion("ARTICLE_PATH")
	
	// MARK: Call Services
	
	public func fetch(_ successClosure: @escaping SuccessClosure<[CategoryModel]>, errorClosure: ErrorClosure? = nil) {
		
		let url = "\(path)/categories"
		
		// Parameters
		let queryItems = [URLQueryItem]()
		
		// Call Service
		self.call(queryItems, suffixPath: url, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	public func fetch(categoryKey: String, pageNo: Int, _ successClosure: @escaping SuccessClosure<[ListModel]>, errorClosure: ErrorClosure? = nil) {
		
		// Parameters
		var queryItems = [URLQueryItem]()
		queryItems.append(URLQueryItem(name: Parameter.category.rawValue, value: categoryKey))
		queryItems.append(URLQueryItem(name: Parameter.page.rawValue, value: String(pageNo)))
		queryItems.append(URLQueryItem(name: Parameter.size.rawValue, value: String(50)))

		// Call Service
		self.call(queryItems, suffixPath: path, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	public func fetch(tagKey: String, pageNo: Int, _ successClosure: @escaping SuccessClosure<[ListModel]>, errorClosure: ErrorClosure? = nil) {
		
		// Parameters
		var queryItems = [URLQueryItem]()
		queryItems.append(URLQueryItem(name: Parameter.tags.rawValue, value: tagKey))
		queryItems.append(URLQueryItem(name: Parameter.page.rawValue, value: String(pageNo)))
		queryItems.append(URLQueryItem(name: Parameter.size.rawValue, value: String(50)))
		
		// Call Service
		self.call(queryItems, suffixPath: path, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	// MARK: Indicator Strategy
	
	public func indicatorStrategy(_ status: IndicatorStrategyResult) -> ArticleService {
		self.indicatorStrategy = status
		return self
	}
}
