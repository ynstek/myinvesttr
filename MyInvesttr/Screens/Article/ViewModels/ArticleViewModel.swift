//
//  ArticleViewModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 22.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

final class ArticleViewModel: MIViewModel {

	// MARK: Properties
	
	let service = ArticleService()
	
	// MARK: Init Methods

	override init() {
		// Initialize service
		super.init()
	}
}
