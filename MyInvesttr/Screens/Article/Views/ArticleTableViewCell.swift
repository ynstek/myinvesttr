//
//  ArticleTableViewCell.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 22.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class ArticleTableViewCell: UITableViewCell {

	// MARK: - Outlets
	
	@IBOutlet var img: UIImageView!
	@IBOutlet var title: UILabel!
	@IBOutlet var authorName: UILabel!
	@IBOutlet var date: UILabel!

	public func configureModel(_ model: ListModel) {
		self.img.downloadImage(model.author?.authorAvatarUrl, makeRound: true)
		self.title.text = model.properties?.title ?? ""
		self.authorName.text = model.author?.authorName ?? ""
		self.date.text = model.publishDate?.toString(format: MICommonConstants.defaultDisplayDateTimeLongFormat)
	}
}
