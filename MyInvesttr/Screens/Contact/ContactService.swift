//
//  ContactService.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 29.05.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final public class ContactService: MIService {

	// MARK: Properties
	
	private let path = MIConfiguration().configForKey("CONTACT_PATH") as! String
	
	// MARK: Call Services
	
	public func fetch(requestModel: ContactRequestModel, _ successClosure: @escaping SuccessClosure<ContactResponseModel>, errorClosure: ErrorClosure? = nil) {
		
		// Call Service
		self.call(requestModel, suffixPath: self.path, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	// MARK: Indicator Strategy
	
	public func indicatorStrategy(_ status: IndicatorStrategyResult) -> ContactService {
		self.indicatorStrategy = status
		return self
	}
}
