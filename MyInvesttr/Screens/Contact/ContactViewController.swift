//
//  ContactViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 28.05.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class ContactViewController: MIMenuViewController {

	// MARK: Outlets

	@IBOutlet var nameTextField: MITextField!
	@IBOutlet var surnameTextField: MITextField!
	@IBOutlet var emailTextField: MITextField!
	@IBOutlet var phoneTextField: MITextField!
	@IBOutlet var messageTextView: MITextView!

	// MARK: Variables
	
	private let service = ContactService()
	
    override func viewDidLoad() {
        super.viewDidLoad()
    }
	
	@IBAction func confirmButton(_ sender: Any) {
		if let model = self.validate() {
			self.service
				.fetch(requestModel: model, { response in
					if response.status == true {
						self.showAlert("İşlem Başarılı", bodyMessage:"Başvurunuz alındı! En kısa zamanda sizi arayacağız." , ok: {
							self.dismiss(animated: true, completion: nil)
						})
					} else {
						self.showAlert("UYARI!", bodyMessage: response.errorMessage ?? "Lütfen bilgilerinizi kontrol ederek tekrar deneyiniz.")
					}
				})
		}
	}
	
	func validate() -> ContactRequestModel? {
		guard let email = self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
			let message = self.messageTextView.text,
			let name = self.nameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
			let surname = self.surnameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
			let phone = self.phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
			else { return nil }
		
		if name.isEmpty, surname.isEmpty, phone.isEmpty, email.isEmpty || message.isEmpty {
			self.showAlert("Boş alanlar var!", bodyMessage: "Lütfen zorunlu alanları boş geçmeyiniz.")
			return nil
		} else if name.count < 3 || surname.count < 3 {
			self.showAlert("Ad veya Soyad alanı doğru değil!", bodyMessage: "Lütfen Ad ve Soyad alanlarını kontrol edip tekrar deneyiniz.")
			return nil
		} else if !phone.isValidPhone() {
			return nil
		} else if !email.isValidEmail() {
			return nil
		}
		
		let registerModel = ContactRequestModel()
		registerModel.name = name
		registerModel.surname = surname
		registerModel.email = email
		registerModel.message = message
		registerModel.phone = phone
		
		return registerModel
	}
}
