//
//  EconomicCalendarViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 9.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import WebKit
import SnapKit

final class EconomicCalendarViewController: MIMenuViewController {
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let webViewC = self.getViewController(storyboardName: .WebView) as! WebViewViewController
		webViewC.itemInfo = self.itemInfo
		webViewC.htmlContentText = """
		<div class="tradingview-widget-container">
		<div class="tradingview-widget-container__widget"></div>
		<div class="tradingview-widget-copyright"></div>
		<script type="text/javascript" src="http://s3.tradingview.com/external-embedding/embed-widget-events.js" async>
		{
		"colorTheme": "light",
		"isTransparent": false,
		"width": "100%",
		"height": "100%",
		"locale": "tr",
		"importanceFilter": "-1,0,1"
		}
		</script>
		</div>
		"""
		
		addChild(webViewC)
		self.view.addSubview(webViewC.view)
		webViewC.didMove(toParent: self)
		
		webViewC.view.snp.makeConstraints { maker in
			maker.trailing.leading.top.bottom.equalToSuperview()
		}
	}	
}
