//
//  ForexCalculateService.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 5.05.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final public class ForexCalculateService: MIService {

	// MARK: Properties
	
	private let forexCalculatepath = MIConfiguration().urlForKeyWithVersion("FOREX_CALCULATE_PATH")
	
	// MARK: Call Services
	
	public func fetch(_ successClosure: @escaping SuccessClosure<[ForexCalculateModel]>, errorClosure: ErrorClosure? = nil) {
		
		// Parameters
		let queryItems = [URLQueryItem]()

		// Call Service
		self.call(queryItems, suffixPath: self.forexCalculatepath, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	// MARK: Indicator Strategy
	
	public func indicatorStrategy(_ status: IndicatorStrategyResult) -> ForexCalculateService {
		self.indicatorStrategy = status
		return self
	}
}
