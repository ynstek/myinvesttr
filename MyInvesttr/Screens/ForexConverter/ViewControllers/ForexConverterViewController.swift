//
//  ForexConverterViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 13.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import SnapKit

final class ForexConverterViewController: MIMenuViewController {

	// MARK: Outlets

	@IBOutlet var view1: UIView!
	@IBOutlet var view2: UIView!
	@IBOutlet var toParityLabel: MILabel!
	@IBOutlet var lastUpdateDate: MILabel!
	
	// MARK: Properties
	
	private let service = ForexCalculateService()
	private var viewModel: ForexCalculateViewModel?
	private var converterView1: ConverterView!
	private var converterView2: ConverterView!
	private var currentDateTime: Date? {
		didSet {
			self.lastUpdateDate.text = self.currentDateTime?.toString(format: "dd MMMM HH:mm:ss")
		}
	}

	// MARK: View lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Prepare UI
		self.prepareUI()
		
		//
		self.toParityLabel.text = ""
		self.lastUpdateDate.text = ""

		// Get List
		self.getList()
	}
	
	private func prepareUI() {
		self.converterView1 = self.createConvertView(selectedIndex: 2)
		self.converterView2 = self.createConvertView(selectedIndex: 0)
		self.addConvertViews()
	}
	
	// MARK: Call Service
	
	func getList() {
		guard self.currentDateTime == nil || Date() >= self.currentDateTime!.date(byAddingSeconds: 10)! else {return}
		self.service.indicatorStrategy(.hide)
			.fetch({ [weak self] response in
			guard let self = self else {return}
			if response.count == 0 {
				self.isExistsList = false
			}
			self.viewModel = ForexCalculateViewModel(response: response)
			self.setToParity()
			self.currentDateTime = Date()
		})
	}
	
	private func setToParity() {
		if let fromSymbol = self.converterView1.selectedForex.name, let toSymbol =  self.converterView2.selectedForex.name {
			let toAmount = self.calculateExchange(fromCurrencyName: fromSymbol, toCurrencyName: toSymbol, amount: 1)
			self.toParityLabel.text = "1 \(fromSymbol) = \(toAmount) \(toSymbol)"
		}
	}
	
	// MARK: Create Convert View
	
	private func createConvertView(selectedIndex: Int) -> ConverterView {
		let converterView = self.getView(nibName: "ConverterView") as! ConverterView
		converterView.configure(selectedIndex: selectedIndex)
		return converterView
	}
	
	private func addConvertViews() {
		self.view1.addSubview(self.converterView1)
		self.view2.addSubview(self.converterView2)
		
		self.converterView1.snp.makeConstraints { make in
			make.edges.equalToSuperview()
		}
		self.converterView2.snp.makeConstraints { make in
			make.edges.equalToSuperview()
		}
		
		self.converterView1.amountTextField.delegate = self
		
		self.converterView2.amountTextField.isUserInteractionEnabled = false
		self.converterView2.amountTextField.placeholder = ""
		self.converterView1.amountTextField.isUserInteractionEnabled = true
		self.converterView1.amountTextField.placeholder = "Tutar Giriniz"
	}
	
	// MARK: Actions
	
	@IBAction func changeButton(_ sender: UIButton) {
		// Remove From Superview
		self.converterView1.removeFromSuperview()
		self.converterView2.removeFromSuperview()
		
		// Change Views
		let tampon = self.converterView1
		self.converterView1 = self.converterView2
		self.converterView2 = tampon
		
		// Add To SuperView
		self.addConvertViews()
		self.setToParity()
		
		// Get List
		self.getList()
	}
	
	// MARK: Calculate Exchange
	
	private func setCalculate(amount: Double) {
		if let fromCurrencyName = self.converterView1.selectedForex.name,
			let toCurrencyName =  self.converterView2.selectedForex.name {
			self.converterView2.amountTextField.text = self.calculateExchange(fromCurrencyName: fromCurrencyName, toCurrencyName: toCurrencyName, amount: amount)
		}
	}
	
	private func calculateExchange(fromCurrencyName: String, toCurrencyName: String, amount: Double) -> String {
		guard let priceList = self.viewModel?.list else {return ""}
		var exchangeFee: Double = 0
		
		let fromCurrencyAmount = priceList.filter {
			price in return price.name == fromCurrencyName
			}.first
		let toCurrencyAmount = priceList.filter {
			price in return price.name == toCurrencyName
			}.first
		
		if let from = fromCurrencyAmount, let to = toCurrencyAmount {
			if from.name == to.name {
				exchangeFee = 1
			} else {
				let toParity = to.end_rate / from.end_rate
				//				let fromParity = from.last / to.last
				exchangeFee = toParity * amount
			}
		}
		
		return exchangeFee.toString()
	}

}

extension ForexConverterViewController: UITextFieldDelegate {
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		
		if let text = textField.text,
			let textRange = Range(range, in: text) {
			let updatedText = text.replacingCharacters(in: textRange,
													   with: string)
			// Get List
			self.getList()
			if let amount = Double(updatedText) {
				self.setCalculate(amount: amount)
			}
		}
		
		return true
	}
}
