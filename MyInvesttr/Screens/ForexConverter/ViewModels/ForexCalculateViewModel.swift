//
//  ForexCalculateViewModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 5.05.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final public class ForexCalculateViewModel: MIViewModel {
	
	// MARK: Properties
	
	public var list: [ForexCalculateViewModel] = []
	public var name : String = ""
	public var end_rate : Double = 0
	public var change : Double = 0
	public var change_pct : Double = 0
	
	// MARK: - Init Methods
	
	public init(response: [ForexCalculateModel]) {
		self.list = [ForexCalculateViewModel]()
		for model in response {
			let modelFromViewModel = ForexCalculateViewModel(response: model)
			self.list.append(modelFromViewModel)
		}
		super.init()
	}
	
	public init(response: ForexCalculateModel) {
		self.name = response.name ?? ""
		self.end_rate = response.end_rate ?? 0
		self.change = response.change ?? 0
		self.change_pct = response.change_pct ?? 0
		super.init()
	}
}
