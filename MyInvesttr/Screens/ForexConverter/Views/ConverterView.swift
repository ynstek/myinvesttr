//
//  ConverterView.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 13.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import SnapKit

final class ConverterView: MIView {

	// MARK: Outlets
	
	@IBOutlet var flagImage: UIImageView!
	@IBOutlet var symbolLabel: MILabel!
	@IBOutlet var forPickerTextField: MITextField!
	@IBOutlet var titleLabel: MILabel!
	@IBOutlet var amountTextField: UITextField!
	@IBOutlet var typeView: UIView!
	
	// MARK: Properties
	
	let pickerView = UIPickerView()
	var selectedForex: ForexConverterModel!
	var forexList = [
		ForexConverterModel(flagName: "flag_TRY", descriptionName: "Türk Lirası", name: "TRY"),
		ForexConverterModel(flagName: "flag_EUR", descriptionName: "Euro", name: "EUR"),
		ForexConverterModel(flagName: "flag_USD", descriptionName: "ABD Doları", name: "USD"),
		ForexConverterModel(flagName: "flag_GBP", descriptionName: "İngiliz Sterlini", name: "GBP"),
		ForexConverterModel(flagName: "flag_JPY", descriptionName: "Japon Yeni", name: "JPY"),
		ForexConverterModel(flagName: "flag_CHF", descriptionName: "İsviçre Frangı", name: "CHF")
	]

	// MARK: Configure
	
	func configure(selectedIndex: Int) {
		self.prepareUI()
		self.pickerView.selectRow(selectedIndex, inComponent: 0, animated: true)
		self.setForex(index: selectedIndex)
	}
	
	// MARK: PrepareUI
	
	func prepareUI() {
		self.amountTextField.text = ""
		self.amountTextField.font = MITheme().font("fontRegular")
		self.pickerView.delegate = self
		self.forPickerTextField.inputView = self.pickerView
		self.typeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapTypeView)))
	}
	
	@objc func tapTypeView() {
		self.forPickerTextField.becomeFirstResponder()
	}

	func setForex(index: Int) {
		self.selectedForex = self.forexList[index]
		self.titleLabel.text = self.selectedForex.descriptionName
		self.symbolLabel.text = self.selectedForex.name
		self.flagImage.image = UIImage(named: self.selectedForex.flagName ?? "")
	}
}

// MARK: UIPickerView

extension ConverterView: UIPickerViewDataSource, UIPickerViewDelegate {
	
	// UIPickerViewDataSource
	
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return self.forexList.count
	}
	
	// UIPickerViewDelegate

	func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
		return 40
	}
	
	func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
		
		let myView = UIView(frame: CGRect.zero)
		let myImageView = UIImageView(frame: CGRect.zero)
		myImageView.contentMode = .scaleAspectFit
		
		var rowString = String()
		let data = self.forexList[row]
		rowString = data.descriptionName ?? ""
		myImageView.image = UIImage(named:data.flagName ?? "")
		
		let myLabel = UILabel(frame: CGRect.zero)
		myLabel.font = MITheme().font("fontRegular")
		myLabel.numberOfLines = 0
		myLabel.text = rowString
		
		myView.addSubview(myLabel)
		myView.addSubview(myImageView)
	
		myImageView.snp.makeConstraints { make in
			make.top.bottom.equalToSuperview()
			make.leading.equalTo(60)
			// Aspect Ratio
			make.width.equalTo(myImageView.snp.height).multipliedBy(51 / 34)
		}
		
		myLabel.snp.makeConstraints { make in
			make.top.bottom.equalToSuperview()
			make.leading.equalTo(myImageView.snp.trailing).offset(20)
		}
		
		return myView
	}
	
	public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		self.setForex(index: row)
	}
}
