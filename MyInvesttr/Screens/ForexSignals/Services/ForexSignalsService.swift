//
//  ForexSignalsService.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 30.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class ForexSignalsService: MIService {
	
	// MARK: Properties
	
	let path = MIConfiguration().urlForKeyWithVersion("FOREX_SIGNALS_PATH")

	// MARK: Call Services
	
	public func fetch(pageNo: Int, _ successClosure: @escaping SuccessClosure<[ListModel]>, errorClosure: ErrorClosure? = nil) {
		
		// Parameters
		var queryItems = [URLQueryItem]()
		queryItems.append(URLQueryItem(name: Parameter.page.rawValue, value: String(pageNo)))
		queryItems.append(URLQueryItem(name: Parameter.size.rawValue, value: String(50)))
		
		// Call Service
		self.call(queryItems, suffixPath: path, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	// MARK: Indicator Strategy
	
	public func indicatorStrategy(_ status: IndicatorStrategyResult) -> ForexSignalsService {
		self.indicatorStrategy = status
		return self
	}
}
