//
//  ForexSignalsViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 30.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class ForexSignalsViewController: MIMenuViewController {

	// MARK: Outlers
	
	@IBOutlet var tableView: UITableView!
	
	// MARK: Variables
	private var viewModel: ForexSignalsViewModel!
	private var list: [ListModel] = []
	private var selectedModel: ListModel!
	private let cellIdentifier = ViewIdConstants.MarketTableViewCell

    override func viewDidLoad() {
        super.viewDidLoad()
		
		// PrepareUI
		self.prepareUI()
		
		// Get List
		self.getList()
    }
	
	// MARK: Prepare UI
	
	func prepareUI() {
		self.viewModel = ForexSignalsViewModel()
		
		// Prepare TableView
		self.tableView.delegate = self
		self.tableView.dataSource = self
		self.tableView.rowHeight = 54
		self.registerTableViewNibs(cellIdentifier, tableView: self.tableView)
	}
	
	// MARK: Call Service
	
	func getList(isFirstPage: Bool = true) {
		if isFirstPage {
			self.selectedPageNo = 1
			self.isExistsList = true
			
			self.viewModel.service
				.fetch(pageNo: self.selectedPageNo, { [weak self] response in
					guard let self = self else {return}
					if response.count == 0 {
						self.isExistsList = false
					}
					self.list = response
					self.tableView.reloadData()
				})
		} else if isExistsList {
			self.selectedPageNo += 1
			
			self.viewModel.service
				.indicatorStrategy(.hide)
				.fetch(pageNo: self.selectedPageNo, { [weak self] response in
					guard let self = self else {return}
					if response.count == 0 {
						self.isExistsList = false
					}
					DispatchQueue.main.async() {
						self.list += response
						self.tableView.reloadData()
					}
				})
		}
	}
}

// MARK: - TableView

extension ForexSignalsViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.list.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
	
		let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.rawValue, for: indexPath) as! MarketTableViewCell
		let item = self.list[indexPath.row].properties

		cell.configureModel(
			name: item?.symbol ?? "",
			value1: item?.currentPrice ?? "",
			value2: item?.option1 ?? "",
			value3: item?.option2 ?? "",
			value4: item?.stopLoss ?? "",
			value5: item?.takeProfit ?? ""
		)
		
		cell.backgroundColor = indexPath.row % 2 == 0 ? self.theme.color("grayLightColor") : self.theme.color("white")
		
		switch item?.process {
		case "-1":
			cell.prosessStatusView.backgroundColor = self.theme.color("redColor")
		case "0":
			cell.prosessStatusView.backgroundColor  = self.theme.color("clear")
		case "1":
			cell.prosessStatusView.backgroundColor  = self.theme.color("greenColor")
		default:
			cell.prosessStatusView.backgroundColor  = self.theme.color("clear")
		}

		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		// Get which cell is selected on the tableview
		self.selectedModel = self.list[indexPath.row];
		
		// Deselect item
		tableView.deselectRow(at: indexPath, animated: true)
		
		// Open Detail
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 44
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let identifier = ViewIdConstants.MarketHeaderTableViewCell

		let headerView = Bundle.main.loadNibNamed(identifier.rawValue, owner: self, options: nil)?.first as! MarketHeaderTableViewCell
		headerView.configureModel(
			name: "Sembol",
			value1: "Fiyat",
			value2: "Option1",
			value3: "Option2",
			value4: "Stop",
			value5: "Take"
		)
		return headerView
	}
}

// MARK: - Scroll

extension ForexSignalsViewController: UIScrollViewDelegate {
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if scrollView.contentOffset.x == 0 && scrollView.contentOffset.y != 0{
			if (scrollView.contentOffset.y == (scrollView.contentSize.height - scrollView.frame.size.height)) {
				self.getList(isFirstPage: false)
			}
		}
	}
}
