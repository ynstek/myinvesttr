//
//  ForexSignalsViewModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 30.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

final class ForexSignalsViewModel: MIViewModel {

	// MARK: Properties
	
	let service = ForexSignalsService()
	
	// MARK: Init Methods
	
	override init() {
		// Initialize service
		super.init()
	}
}
