//
//  MainTabBarController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 6.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import FirebaseMessaging

final class MainTabBarController: UITabBarController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		MIFirebase().firebaseSetTokenAndTopic()
	}
}
