//
//  MenuViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 24.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class MenuViewController: MIViewController {

	// MARK: Outlets
	
	@IBOutlet var headerView: UIView!
	@IBOutlet var tableView: UITableView!
	@IBOutlet var usernameLabel: MILabel!
	@IBOutlet var loginButton: MIButton!
	@IBOutlet var registerButton: MIButton!
	
	// MARK: Variables
	
	var list: [MenuModel]!
	
	override func viewDidLoad() {
        super.viewDidLoad()
	
		// PrepareUI
		self.prepareUI()
	}
	
	func prepareUI() {
		self.tableView.delegate = self
		self.tableView.rowHeight = 40

		self.headerView.backgroundColor = self.theme.color("greenDarkColor")
		self.registerButton.backgroundColor = self.theme.color("blueColor")
		
		// Create Menu Items
		self.list = [
			MenuModel(title: "Piyasalar", imageName: "iconMarket", identifier: .home),
			MenuModel(title: "Sinyaller", imageName: "iconFxSignal", identifier: .forexSignals),
			MenuModel(title: "Döviz Çevirici", imageName: "iconCalculator", identifier: .forexConverter),
			MenuModel(title: "Teknik Analiz", imageName: "iconMarket", identifier: .technicalAnalysis),
			MenuModel(title: "Video Analiz", imageName: "iconMarket", identifier: .videoAnalysis),
			MenuModel(title: "Ekonomik Takvim", imageName: "iconCalendar", identifier: .economicCalendar),
			MenuModel(title: "İletişim", imageName: "favicon", identifier: .contact)
		]
		
		self.loginButton.setTitle("Giriş Yap", for: .normal)
		self.registerButton.setTitle("Üye Ol", for: .normal)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		// User
		if let modelData = UserDefaults.standard.object(forKey: "LoginResponseModel") as? Data,
			let loginResponseModel = try? PropertyListDecoder().decode(LoginResponseModel.self, from: modelData) {
			self.usernameLabel.text = "\(loginResponseModel.firstname ?? "") \(loginResponseModel.lastname ?? "")"
			self.setLoginHidden(true)
		} else {
			self.setLoginHidden(false)
		}
	}
	
	func prepareSignOutCell(_ status: Bool) {
		if status {
			if self.list.last?.identifier != .signOut {
				self.list.insert(MenuModel(title: "Çıkış Yap", imageName: "iconSignOut", identifier: .signOut), at: self.list.count)
				self.tableView.reloadData()
			}
		} else {
			if self.list.last?.identifier == .signOut {
				self.list.removeLast()
				self.tableView.reloadData()
			}
		}
	}
	
	func setLoginHidden(_ status: Bool) {
		self.loginButton.isHidden = status
		self.registerButton.isHidden = status
		self.usernameLabel.isHidden = !status
		
		self.prepareSignOutCell(status)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		super.prepare(for: segue, sender: sender)
		guard let menu = sender as? MenuModel else {return}

		if MenuSegueIdentifier(rawValue: segue.identifier!)! != .home {
			var controller = MIMenuViewController()

			if let navController = segue.destination as? MINavigationController {
				navController.prepareNavBar()
				controller = navController.firstApplicableViewController() as! MIMenuViewController
			} else {
				controller = segue.destination as! MIMenuViewController
			}
			
			controller.menuSegueIdentifier = MenuSegueIdentifier(rawValue: segue.identifier!)
			controller.title = menu.title
			controller.menuSegueIdentifier = menu.identifier
		}
	}
	
	private func isCurrentViewController(_ identifier: MenuSegueIdentifier) -> Bool {
		let currentVC = self.revealViewController()?.frontViewController
		
		switch identifier {
		case .home:
			if currentVC is MainTabBarController {
				return true
			}
			break
		case .forexSignals:
			if (currentVC as? MINavigationController)?.firstApplicableViewController() is ForexSignalsViewController {
				return true
			}
			break
		case .technicalAnalysis:
			if let vc = (currentVC as? MINavigationController)?.firstApplicableViewController(), vc is AnalysisViewController, (vc as! AnalysisViewController).menuSegueIdentifier == .technicalAnalysis {
				return true
			}
			break
		case .videoAnalysis:
			if let vc = (currentVC as? MINavigationController)?.firstApplicableViewController(), vc is AnalysisViewController, (vc as! AnalysisViewController).menuSegueIdentifier == .videoAnalysis {
				return true
			}
			break
		case .economicCalendar:
			if let vc = (currentVC as? MINavigationController)?.firstApplicableViewController(), vc is EconomicCalendarViewController {
				return true
			}
			break
		case .forexConverter:
			if let vc = (currentVC as? MINavigationController)?.firstApplicableViewController(), vc is ForexConverterViewController {
				return true
			}
			break
		case .contact:
			if let vc = (currentVC as? MINavigationController)?.firstApplicableViewController(), vc is ContactViewController {
				return true
			}
			break
		case .signOut:
			break
		}
		return false
	}
	
	@IBAction func loginButton(_ sender: Any?) {
		self.closeMenu()
		self.present(storyboardName: .Account, viewControllerId: .Login)
	}
	
	@IBAction func registerButton(_ sender: Any) {
		self.closeMenu()
		self.present(storyboardName: .Account, viewControllerId: .Register)
	}
	
	func closeMenu() {
		// Close Menu
		self.revealViewController().setFrontViewPosition(.left, animated: true)
	}
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.list.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		self.tableView.rowHeight = 54
		
		let identifier = ViewIdConstants.MenuTableViewCell
		self.registerTableViewNibs(identifier, tableView: self.tableView)
		let cell = tableView.dequeueReusableCell(withIdentifier: identifier.rawValue, for: indexPath) as! MenuTableViewCell
		let item = self.list[indexPath.row]
		cell.configure(model: item,iconTintColor: self.theme.color("green"))
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		// Get which cell is selected on the tableview
		let selectedModel = self.list[indexPath.row];
		
		// Deselect item
		tableView.deselectRow(at: indexPath, animated: true)

		if let identifier = selectedModel.identifier {
			if identifier == .signOut {
				self.showAlert("Çıkış Yap", bodyMessage: "Çıkış yapmak istediğinize emin misiniz?", yes: {
					self.closeMenu()
					// LogOut
					UserDefaults.standard.removeObject(forKey: "LoginResponseModel")
					UserDefaults.standard.synchronize()
				})
			} else if isCurrentViewController(identifier) {
				self.closeMenu()
			} else {
				// If Login
				if (UserDefaults.standard.object(forKey: "LoginResponseModel") as? Data) != nil {
					// Open Segue
					self.performSegue(withIdentifier: identifier.rawValue, sender: selectedModel)
				} else {
					if let identifier = selectedModel.identifier,
						identifier != .forexSignals,
						identifier != .technicalAnalysis,
						identifier != .videoAnalysis {
						// Open Segue
						self.performSegue(withIdentifier: identifier.rawValue, sender: selectedModel)
					} else {
						self.loginButton(nil)
					}
				}

			}
		}
	}
}
