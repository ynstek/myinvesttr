//
//  MenuTableViewCell.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 25.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class MenuTableViewCell: UITableViewCell {
	
	// MARK: Outlets
	
	@IBOutlet var iconImageView: UIImageView!
	@IBOutlet var titleLabel: MILabel!
	
	func configure(model: MenuModel, iconTintColor: UIColor) {
		self.titleLabel.text = model.title
		self.iconImageView?.image = UIImage(named: model.imageName!)
		self.iconImageView?.tintColor = iconTintColor
	}
}
