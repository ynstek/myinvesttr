//
//  MarketService.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 31.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final public class MarketService: MIService {

	// MARK: Properties
	
	var path = MIConfiguration().configForKey("QUOTES_PATH") as! String
	var version = MIConfiguration().configForKey("QUOTES_VERSION") as! String
	
	// MARK: Call Services
	
	public func fetch(categoryKey: String, _ successClosure: @escaping SuccessClosure<[MarketModel]>, errorClosure: ErrorClosure? = nil) {
		
		// Url
		let url = "\(path)/\(categoryKey)/\(version)"

		// Parameters
		var queryItems = [URLQueryItem]()
		queryItems.append(URLQueryItem(name: Parameter.size.rawValue, value: String(50)))
		
		// Call Service
		self.call(queryItems, suffixPath: url, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	// MARK: Indicator Strategy
	
	public func indicatorStrategy(_ status: IndicatorStrategyResult) -> MarketService {
		self.indicatorStrategy = status
		return self
	}
}
