//
//  MarketCategoryViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 7.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import XLPagerTabStrip

final class MarketCategoryViewController: MIViewController {

	// MARK: Outlets
	
	@IBOutlet var tableView: UITableView!
	
	// MARK: Variables
	
	private var service = MarketService()
	private var list: [MarketModel] = []
	private var selectedModel: MarketModel!
	private let cellIdentifier = ViewIdConstants.MarketTableViewCell
	private let refreshControl = UIRefreshControl()

	override func viewDidLoad() {
		super.viewDidLoad()
		
		// PrepareUI
		self.prepareUI()
		
		// Get List
		self.getList()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.refreshControl.endRefreshing()
	}
	
	// MARK: Prepare UI
	
	fileprivate func prepareUI() {
		// Prepare TableView
		
		self.tableView.delegate = self
		self.tableView.dataSource = self
		if MarketCategory(rawValue: self.itemInfo.title ?? "") == .gold {
			self.tableView.rowHeight = 74
		} else {
			self.tableView.rowHeight = 54
		}
		self.registerTableViewNibs(self.cellIdentifier, tableView: self.tableView)
		
		// Add Refresh Control to Table View
		if #available(iOS 10.0, *) {
			self.tableView.refreshControl = refreshControl
		} else {
			self.tableView.addSubview(refreshControl)
		}
		
		// Configure Refresh Control
		self.refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
	}
	
	@objc private func refreshWeatherData(_ sender: Any) {
		// Fetch Weather Data
		self.getList(false)
	}
	
	// MARK: Call Service
	
	func getList(_ isFirstCall: Bool = true) {
		if let key = MarketCategory(rawValue: self.itemInfo.title ?? "")?.categoryKey() , !key.isEmpty {
			
			if isFirstCall {
				if key != MarketCategory.forex.categoryKey() {
					self.service = service.indicatorStrategy(.hide)
				}
			}
			
			self.service.fetch(categoryKey: key, { [weak self] response in
				guard let self = self else { return }
				
				let category = MarketCategory(rawValue: self.itemInfo.title ?? "")
				if category == .cryptoCurrency {
					self.list = Array(response.filter(){
						cryptoCurrency in
						return cryptoCurrency.dailyChange != 0
						}.prefix(25))
				} else {
					self.list = response
				}
				
				self.refreshControl.endRefreshing()
				self.tableView.reloadData()
			})
		}
	}
	
	func setChangeColor(prefix: String, changeValue: Double?, label: UILabel) {
		guard let changeValue = changeValue else {return}
		
		if changeValue < 0.0 {
			label.text = "—\(prefix)\(abs(changeValue).toString())" // ▼
			label.textColor = self.theme.color("redColor")
		} else {
			label.text = "+\(prefix)\(abs(changeValue).toString())" // ▲
			label.textColor = self.theme.color("greenDarkColor")
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		super.prepare(for: segue, sender: sender)
		if segue.identifier == "Detail" {
			if let detailViewController = segue.destination as? MarketDetailViewController {
				detailViewController.title = self.selectedModel.symbol ?? ""
				detailViewController.model = self.selectedModel
				detailViewController.category = MarketCategory(rawValue: self.itemInfo.title ?? "")
			}
		}
	}
}

// MARK: - TableView

extension MarketCategoryViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.list.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.rawValue, for: indexPath) as! MarketTableViewCell
		let item = self.list[indexPath.row]
		
		let category = MarketCategory(rawValue: self.itemInfo.title ?? "")
		if category == .forex {
			cell.configureModel(
				name: item.symbol ?? " ",
				value1: item.bid?.toString() ?? " ",
				value2: item.last?.toString() ?? " ",
				value3: item.dailyChangePercent?.toString() ?? " "
			)
			self.setChangeColor(prefix: "%", changeValue: item.dailyChangePercent, label: cell.value3)
		} else if category == .cryptoCurrency {
			cell.configureModel(
				iconUrlString: item.imageUrl ?? "",
				name: item.symbol ?? " ",
				value1: item.last != nil ? "$" + (item.last?.toString())! : " ",
				value2: item.dailyChange?.toString() ?? " ",
				nameWidth: 110
			)
			self.setChangeColor(prefix: "", changeValue: item.dailyChange, label: cell.value2)
		} else if category == .gold {
			cell.configureModel(
				name: item.securityDesc ?? " ",
				value1: item.bid?.toString() ?? " ",
				value2: item.last?.toString() ?? " ",
				value3: item.dailyChange?.toString() ?? " ",
				nameWidth: 120
			)
			self.setChangeColor(prefix: "", changeValue: item.dailyChange, label: cell.value3)
		}
		
		cell.backgroundColor = indexPath.row % 2 == 0 ? self.theme.color("grayLight") : self.theme.color("white")
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let identifier = ViewIdConstants.MarketHeaderTableViewCell
		
		let headerView = Bundle.main.loadNibNamed(identifier.rawValue, owner: self, options: nil)?.first as! MarketHeaderTableViewCell
		let category = MarketCategory(rawValue: self.itemInfo.title ?? "")
		if category == .forex {
			headerView.configureModel(
				name: "Sembol",
				value1: "Açılış",
				value2: "Fiyat",
				value3: "Değişim"
			)
		} else if category == .cryptoCurrency {
			headerView.configureModel(
				name: "Sembol",
				value1: "Fiyat",
				value2: "Değişim",
				nameWidth: 110
			)
		} else if category == .gold {
			headerView.configureModel(
				name: "Sembol",
				value1: "Alış",
				value2: "Satış",
				value3: "Değişim",
				nameWidth: 120
			)
		}
		
		return headerView
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		// Get which cell is selected on the tableview
		self.selectedModel = self.list[indexPath.row];
		
		// Deselect item
		tableView.deselectRow(at: indexPath, animated: true)
		
		self.performSegue(withIdentifier: "Detail", sender: nil)
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 44
	}
}
