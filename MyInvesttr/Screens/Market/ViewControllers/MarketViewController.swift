//
//  MarketViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 7.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import XLPagerTabStrip

final class MarketViewController: MIPagerViewController {
	
    override func viewDidLoad() {
        super.viewDidLoad()
    }
	
	// MARK: - PagerTabStripDataSource
	
	override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
		let categories: [String] = [MarketCategory.forex.rawValue, MarketCategory.cryptoCurrency.rawValue, MarketCategory.gold.rawValue]
		
		var childs: [UIViewController] = []
		for category in categories {
			let child = self.getViewController(storyboardName: .Market, viewControllerId: .MarketCategory) as? MarketCategoryViewController
			child!.itemInfo = IndicatorInfo(title: category)
			childs.append(child!)
		}
		
		return childs
	}
}
