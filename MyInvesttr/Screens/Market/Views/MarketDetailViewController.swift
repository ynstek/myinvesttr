//
//  MarketDetailViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 2.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import XLPagerTabStrip

final class MarketDetailViewController: MIPagerViewController {

	// MARK: Outlets
	
	@IBOutlet var title1: MILabel!
	@IBOutlet var value1: MILabel!
	@IBOutlet var title2: MILabel!
	@IBOutlet var value2: MILabel!
	@IBOutlet var stackView2: UIStackView!
	@IBOutlet var title3: MILabel!
	@IBOutlet var value3: MILabel!
	@IBOutlet var priceView: UIView!
	
	// MARK: Properties
	
	var model: MarketModel!
	var category: MarketCategory!
	
	// MARK: Properties
	
    override func viewDidLoad() {
		self.prepareNavBar()
        super.viewDidLoad()
		self.prepareUI()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		self.haveMenuButton = false
		super.viewWillAppear(animated)
	}
	
	func prepareUI() {
		if category == .forex {
			self.title1.text = "Alış"
			self.value1.text = self.model.bid?.toString() ?? ""
			self.title2.text = "Satış"
			self.value2.text = self.model.last?.toString() ?? ""
			self.title3.text = "Değişim"
			
			let changeValue = self.model.dailyChangePercent ?? 0
			let label: UILabel = self.value3
			
			if changeValue < 0.0 {
				label.text = "—%\(abs(changeValue).toString())" // ▼
			} else {
				label.text = "+%\(abs(changeValue).toString())" // ▲
			}
		} else if category == .cryptoCurrency {
			self.title1.text = "Fiyat"
			self.value1.text = self.model.last?.toString() ?? ""
			self.title3.text = "Değişim"
			
			let changeValue = self.model.dailyChange ?? 0
			let label: UILabel = self.value3
			
			if changeValue < 0.0 {
				label.text = "—\(abs(changeValue).toString())" // ▼
			} else {
				label.text = "+\(abs(changeValue).toString())" // ▲
			}
		} else if category == .gold {
			self.title1.text = "Fiyat"
			self.value1.text = self.model.last?.toString() ?? ""
			self.title3.text = "Değişim"
			
			let changeValue = self.model.dailyChange ?? 0
			let label: UILabel = self.value3
			
			if changeValue < 0.0 {
				label.text = "—\(abs(changeValue).toString())" // ▼
			} else {
				label.text = "+\(abs(changeValue).toString())" // ▲
			}
		}

		self.stackView2.isHidden = self.title2.text!.isEmpty ? true : false
	}
	
	func prepareNavBar() {
		// Set NavBar Color
		var color = self.theme.color("greenDarkColor")
		if let chg = self.model.dailyChangePercent {
			color = chg < 0 ? self.theme.color("redColor") : color
		}
		
		self.priceView.backgroundColor = color
		self.navigationBackgroundColor = color
		self.barbuttonBackgroundColor = color
	}
	
	// MARK: - PagerTabStripDataSource
	
	override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
		
		var childs: [UIViewController] = []
		
		if let child = self.getViewController(storyboardName: .WebView) as? WebViewViewController {
			child.itemInfo = IndicatorInfo(title: MarketDetailCategory.datas.rawValue)
			
			if self.category == MarketCategory.forex || self.category == MarketCategory.cryptoCurrency {
				child.urlString = "http://api.myinvesttr.com/forex/Chart/v1?q=\(self.model.chart?.q ?? "")&q1=\(self.model.chart?.q1 ?? "")"
			} else {
				child.htmlContentText = """
				<div class="tradingview-widget-container">
				<div class="tradingview-widget-container__widget"></div>
				<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js" async>
				{
				"symbol": "FX:\(self.model.symbol ?? "")",
				"width": "100%",
				"height": "100%",
				"locale": "tr",
				"dateRange": "all",
				"colorTheme": "light",
				"trendLineColor": "#37a6ef",
				"underLineColor": "#e3f2fd",
				"isTransparent": false,
				"autosize": true,
				"largeChartUrl": ""
				}
				</script>
				</div>
				"""
			}
			childs.append(child)
		}
		
		if let child = self.getViewController(storyboardName: .NewsList) as? NewsListViewController {
			child.itemInfo = IndicatorInfo(title: MarketDetailCategory.news.rawValue)
			child.tagKey = self.model.symbol ?? ""
			child.haveHeaderCell = false
			childs.append(child)
		}

		if let child = self.getViewController(storyboardName: .ArticleList) as? ArticleListViewController {
			child.itemInfo = IndicatorInfo(title: MarketDetailCategory.articles.rawValue)
			child.tagKey = self.model.symbol ?? ""
			childs.append(child)
		}
		
		return childs
	}
}
