//
//  NewsService.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 12.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Alamofire
import Foundation

final class NewsService: MIService {
	
	// MARK: Properties
	
	let path = MIConfiguration().urlForKeyWithVersion("NEWS_PATH")
	
	// MARK: Call Services
	
	public func fetch(_ successClosure: @escaping SuccessClosure<[CategoryModel]>, errorClosure: ErrorClosure? = nil) {
		
		// Parameters
		let url = "\(path)/categories"
		
		// Parameters
		let queryItems = [URLQueryItem]()
		
		// Call Service
		self.call(queryItems, suffixPath: url, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	public func fetch(categoryKey: String, pageNo: Int, _ successClosure: @escaping SuccessClosure<[ListModel]>, errorClosure: ErrorClosure? = nil) {
		
		// Parameters
		var queryItems = [URLQueryItem]()
		queryItems.append(URLQueryItem(name: Parameter.category.rawValue, value: categoryKey))
		queryItems.append(URLQueryItem(name: Parameter.page.rawValue, value: String(pageNo)))
		queryItems.append(URLQueryItem(name: Parameter.size.rawValue, value: String(50)))
		
		// Call Service
		self.call(queryItems, suffixPath: self.path, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	public func fetch(tagKey: String, pageNo: Int, _ successClosure: @escaping SuccessClosure<[ListModel]>, errorClosure: ErrorClosure? = nil) {
		
		// Parameters
		var queryItems = [URLQueryItem]()
		queryItems.append(URLQueryItem(name: Parameter.tags.rawValue, value: tagKey))
		queryItems.append(URLQueryItem(name: Parameter.page.rawValue, value: String(pageNo)))
		queryItems.append(URLQueryItem(name: Parameter.size.rawValue, value: String(50)))
		
		// Call Service
		self.call(queryItems, suffixPath: self.path, successClosure: successClosure, errorClosure: errorClosure)
	}
	
	// MARK: Indicator Strategy
	
	public func indicatorStrategy(_ status: IndicatorStrategyResult) -> NewsService {
		self.indicatorStrategy = status
		return self
	}

}
