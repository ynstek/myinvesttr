//
//  NewsViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 7.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import XLPagerTabStrip

final class NewsViewController: MIPagerViewController {

	// MARK: Variables
	
	private var viewModel: NewsViewModel!
	private var categories: [CategoryModel] = []
	
	// MARK: View Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.prepareUI()
		self.getCategories()
	}
	
	// MARK: Prepare UI

	func prepareUI() {
		self.viewModel = NewsViewModel()
	}
	
	// MARK: Call Service

	func getCategories() {
		self.viewModel.service.fetch( { [weak self] (response) in
			guard let self = self else {return}
			self.categories = response
			self.reloadPagerTabStripView()
		})
	}
	
	// MARK: - PagerTabStripDataSource
	
	override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {

		guard self.categories.count != 0 else {
			return [self.getViewController(storyboardName: .NewsList, viewControllerId: .NewsList) as! NewsListViewController]
		}
		 
		var childs: [UIViewController] = []
		for i in self.categories {
			let child = self.getViewController(storyboardName: .NewsList, viewControllerId: .NewsList) as! NewsListViewController
			child.itemInfo = IndicatorInfo(title: i.name)
			child.categoryKey = i.key ?? i.name ?? ""
			childs.append(child)
		}
		
		return childs
	}
}
