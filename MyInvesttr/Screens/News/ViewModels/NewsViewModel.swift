//
//  NewsViewModel.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 12.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import Foundation

final class NewsViewModel: MIViewModel {

	// MARK: Properties
	
	let service = NewsService()
	
	// MARK: Init Methods
	
	override init() {
		// Initialize service
		super.init()
	}
}
