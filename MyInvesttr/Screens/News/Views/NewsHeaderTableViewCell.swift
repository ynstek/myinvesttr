//
//  NewsHeaderTableViewCell.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 16.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import Kingfisher

final class NewsHeaderTableViewCell: UITableViewCell {

	// MARK: - Outlets
	
	@IBOutlet var img: UIImageView!
	@IBOutlet var title: UILabel!
	@IBOutlet var date: UILabel!
	
	public func configureModel(_ model: ListModel) {
		self.img.downloadImage(model.properties?.imagePath)
		self.title.text = model.properties?.title ?? ""
		self.date.text = model.publishDate?.toString(format: MICommonConstants.defaultDisplayDateTimeLongFormat)
	}

	override func draw(_ rect: CGRect) {
		super.draw(rect)
		self.img.setGradientBackground(one: UIColor.black, two: UIColor.clear, pointPozition: .bottomToTop, alpha: 0.85)
	}
}
