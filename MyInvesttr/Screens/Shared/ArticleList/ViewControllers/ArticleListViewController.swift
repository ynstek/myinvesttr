//
//  ArticleListViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 22.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import DZNEmptyDataSet

final class ArticleListViewController: MIViewController {

	// MARK: Outlets
	
	@IBOutlet var tableView: UITableView!
	
	// MARK: Variables
	
	private var viewModel: ArticleViewModel!
	private var list: [ListModel]!
	var selectedModel: ListModel!

	override func viewDidLoad() {
		super.viewDidLoad()
		
		// PrepareUI
		self.prepareUI()
		
		// Get List
		self.getList()
	}
	
	// MARK: Prepare UI
	
	fileprivate func prepareUI() {
		self.viewModel = ArticleViewModel()
		
		// Prepare TableView
		
		self.tableView.delegate = self
		self.tableView.dataSource = self
		self.tableView.emptyDataSetSource = self
		self.registerTableViewNibs(.ArticleTableViewCell, tableView: self.tableView)
		self.tableView.rowHeight = 80
	}
	
	// MARK: Call Service
	
	func getList(isFirstPage: Bool = true) {
		if !self.categoryKey.isEmpty {
			if isFirstPage {
				self.selectedPageNo = 1
				self.isExistsList = true
				
				var service = self.viewModel.service
				if self.categoryKey != MarketCategory.forex.categoryKey() {
					service = service.indicatorStrategy(.hide)
				}
				
				service.fetch(categoryKey: self.categoryKey, pageNo: self.selectedPageNo, { [weak self] response in
						guard let self = self else {return}
						if response.count == 0 {
							self.isExistsList = false
						}
						self.list = response
						self.tableView.reloadData()
					})
				
			} else if isExistsList {
				self.selectedPageNo += 1
				
				self.viewModel.service
					.indicatorStrategy(.hide)
					.fetch(categoryKey: self.categoryKey, pageNo: self.selectedPageNo, { [weak self] response in
						guard let self = self else {return}
						if response.count == 0 {
							self.isExistsList = false
						}
						DispatchQueue.main.async() {
							self.list += response
							self.tableView.reloadData()
						}
					})
			}
		} else if !self.tagKey.isEmpty {
			let service = self.viewModel.service.indicatorStrategy(.hide)

			service.fetch(tagKey: self.tagKey, pageNo: self.selectedPageNo, { [weak self] response in
				guard let self = self else {return}
				self.list = response
				self.tableView.reloadData()
			})
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		super.prepare(for: segue, sender: sender)
		if segue.identifier == "Detail" {
			if let detailViewController = segue.destination as? ArticleDetailViewController {
				detailViewController.title = self.itemInfo.title
				detailViewController.model = self.selectedModel
			}
		}
	}
}

// MARK: - TableView

extension ArticleListViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.list?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let identifier = ViewIdConstants.ArticleTableViewCell
		self.registerTableViewNibs(identifier, tableView: self.tableView)
		let cell = tableView.dequeueReusableCell(withIdentifier: identifier.rawValue, for: indexPath) as! ArticleTableViewCell
		let item = self.list[indexPath.row]
		cell.configureModel(item)
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		// Get which cell is selected on the tableview
		self.selectedModel = self.list[indexPath.row];
		
		// Deselect item
		tableView.deselectRow(at: indexPath, animated: true)
		
		self.performSegue(withIdentifier: "Detail", sender: nil)
	}
}

// MARK: Empty Data Set

extension ArticleListViewController: DZNEmptyDataSetSource {
	func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
		if self.list != nil {
			let text = "Gösterilecek makale bulunamadı."
			
			let attributes = [
				NSAttributedString.Key.font: self.theme.font("fontMedium"),
				NSAttributedString.Key.foregroundColor: UIColor.darkGray
			]
			
			return NSAttributedString(string: text, attributes: attributes)
		}
		return nil
	}
}

// MARK: - Scroll

extension ArticleListViewController: UIScrollViewDelegate {
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if scrollView.contentOffset.x == 0 && scrollView.contentOffset.y != 0{
			if (scrollView.contentOffset.y == (scrollView.contentSize.height - scrollView.frame.size.height)) {
				self.getList(isFirstPage: false)
			}
		}
	}
}
