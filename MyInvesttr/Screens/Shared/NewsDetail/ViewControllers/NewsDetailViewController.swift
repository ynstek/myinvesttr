//
//  NewsDetailViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 23.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class NewsDetailViewController: MIViewController {

	// MARK: - Outlets
	
	@IBOutlet var titleLabel: UILabel!
	@IBOutlet var img: UIImageView!
	@IBOutlet var date: UILabel!
	@IBOutlet var content: MILabel!

	// MARK: Properties
	
	public var model: ListModel!

    override func viewDidLoad() {
        super.viewDidLoad()
		self.prepareUI()
    }
	
	func prepareUI() {
		if let model = self.model {
			self.titleLabel.text = model.properties?.title ?? ""
			self.img.downloadImage(model.properties?.imagePath)
			self.date.text = model.publishDate?.toString(format: MICommonConstants.defaultDisplayDateTimeLongFormat)
			let htmlContentText = model.properties?.content ?? ""
			DispatchQueue.main.async {
				MIIndicatorPresenter().present()
				self.content.text = ""
				self.content.attributedText = htmlContentText.html2AttributedString
				MIIndicatorPresenter().hide()
			}
			
			self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(self.sharetButton))
		}
	}
	
	@objc func sharetButton() {
		let imageUrl = self.configuration.configForKey("SHARE_NEWS_PREFIX_URL") as! String
		let url = imageUrl + "/" + (self.model.url ?? "")
		let title = self.model.properties?.title ?? ""
		let text = "\(title)\n\(url)"
		
		// set up activity view controller
		let textToShare = [ text ]
		let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
		activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
		// present the view controller
		self.present(activityViewController, animated: true, completion: nil)
	}
	
	
}
