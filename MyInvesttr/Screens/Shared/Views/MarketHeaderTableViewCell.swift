//
//  MarketHeaderTableViewCell.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 30.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class MarketHeaderTableViewCell: UITableViewCell {
	
	// MARK: - Outlets
	
	@IBOutlet var name: MILabel!
	@IBOutlet var value1: MILabel!
	@IBOutlet var value2: MILabel!
	@IBOutlet var value3: MILabel!
	@IBOutlet var value4: MILabel!
	@IBOutlet var value5: MILabel!
	@IBOutlet var value6: MILabel!

	@IBOutlet var nameWidth: NSLayoutConstraint!
	
	public func configureModel(name: String, value1: String = "", value2: String = "", value3: String = "", value4: String = "", value5: String = "", value6: String = "", nameWidth: CGFloat? = nil) {
		self.name.text = name
		self.value1.text = value1
		self.value2.text = value2
		self.value3.text = value3
		self.value4.text = value4
		self.value5.text = value5
		self.value6.text = value6
		
		self.value1.isHidden = value1.isEmpty ? true : false
		self.value2.isHidden = value2.isEmpty ? true : false
		self.value3.isHidden = value3.isEmpty ? true : false
		self.value4.isHidden = value4.isEmpty ? true : false
		self.value5.isHidden = value5.isEmpty ? true : false
		self.value6.isHidden = value6.isEmpty ? true : false
		
		self.setAlighment()
		
		self.nameWidth.constant = nameWidth != nil ? nameWidth! : self.nameWidth.constant
	}
	
	func setAlighment() {
		if self.value2.isHidden {
			self.value1.textAlignment = .right
		} else if self.value3.isHidden {
			self.value2.textAlignment = .right
		} else if self.value4.isHidden {
			self.value3.textAlignment = .right
		} else if self.value5.isHidden {
			self.value4.textAlignment = .right
		} else if self.value6.isHidden {
			self.value5.textAlignment = .right
		}
	}
}
