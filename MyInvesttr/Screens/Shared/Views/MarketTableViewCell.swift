//
//  MarketTableViewCell.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 30.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class MarketTableViewCell: UITableViewCell {
	
	// MARK: Outlets
	
	@IBOutlet var icon: UIImageView!
	@IBOutlet var name: MILabel!
	@IBOutlet var value1: MILabel!
	@IBOutlet var value2: MILabel!
	@IBOutlet var value3: MILabel!
	@IBOutlet var value4: MILabel!
	@IBOutlet var value5: MILabel!
	@IBOutlet var value6: MILabel!

	@IBOutlet var nameWidth: NSLayoutConstraint!
	@IBOutlet var prosessStatusView: UIView!
	
	public func configureModel(iconUrlString: String = "", name: String, value1: String = "", value2: String = "", value3: String = "", value4: String = "", value5: String = "", value6: String = "", nameWidth: CGFloat? = nil) {
		self.icon.downloadImage(iconUrlString, makeRound: true)
		self.icon.isHidden = iconUrlString.isEmpty ? true : false
		self.setLabel(self.name, name)
		self.setLabel(self.value1, value1)
		self.setLabel(self.value2, value2)
		self.setLabel(self.value3, value3)
		self.setLabel(self.value4, value4)
		self.setLabel(self.value5, value5)
		self.setLabel(self.value6, value6)
		
		self.setAlighment()
		self.nameWidth.constant = nameWidth != nil ? nameWidth! : self.nameWidth.constant
	}
	
	private func setLabel(_ label: UILabel, _ text: String) {
		label.text = text
		label.isHidden = text.isEmpty ? true : false
	}
	
	private func setAlighment() {
		if self.value2.isHidden {
			self.value1.textAlignment = .right
		} else if self.value3.isHidden {
			self.value2.textAlignment = .right
		} else if self.value4.isHidden {
			self.value3.textAlignment = .right
		} else if self.value5.isHidden {
			self.value4.textAlignment = .right
		} else if self.value6.isHidden {
			self.value5.textAlignment = .right
		}
	}
}
