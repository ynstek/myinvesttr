//
//  NewsTableViewCell.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 16.03.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class NewsTableViewCell: UITableViewCell {

	// MARK: - Outlets
	
	@IBOutlet var img: UIImageView!
	@IBOutlet var title: UILabel!
	@IBOutlet var date: UILabel!
	@IBOutlet var content: UILabel!
	
	public func configureModel(_ model: ListModel) {
		self.img.downloadImage(model.properties?.imagePath)
		self.title.text = model.properties?.title ?? ""
		self.date.text = model.publishDate?.toString(format: MICommonConstants.defaultDisplayDateTimeLongFormat)
		self.content.text = model.properties?.summary ?? ""
	}
}
