//
//  SplashViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 31.05.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit

final class SplashViewController: MIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		self.present(storyboardName: .Menu)
	}
}
