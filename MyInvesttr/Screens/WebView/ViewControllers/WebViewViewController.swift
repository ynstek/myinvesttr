//
//  WebViewViewController.swift
//  MyInvesttr
//
//  Created by Yunus TEK on 27.04.2019.
//  Copyright © 2019 Myinvesttr. All rights reserved.
//

import UIKit
import WebKit

final class WebViewViewController: MIViewController {

	// MARK: Properties
	
	var webView: WKWebView!
	public var htmlContentText: String!
	public var urlString: String!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.prepareUI()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		self.webView.frame = self.view.frame
	}
	
	func prepareUI() {
		self.webView = WKWebView()
		self.webView.navigationDelegate = self
		self.view.addSubview(self.webView)
		self.webView.frame = self.view.bounds
		self.webView.autoresizesSubviews = true
		self.webView.autoresizingMask = [.flexibleHeight, .flexibleWidth]

		if let urlString = self.urlString {
			if let url = URL(string: urlString) {
				let urlRequset = URLRequest(url: url)
				
				self.webView.load(urlRequset)
				self.webView.allowsBackForwardNavigationGestures = true
			} else {
				self.showAlert("", bodyMessage: "Veri Yüklenemedi", showButton: false)
			}
		} else if let htmlContentText = self.htmlContentText {
			let font = MITheme().font("fontRegular")

			let format = String(format:
				"%@" + "<style>body,h1,h2,h3,h4,h5,h6,p,span,b,strong,i{font-family:'"
				+ "%@" + "'!important;font-size:"
				+ "%fpx" + "!important;}</style>",
								htmlContentText,
								font.fontName,
								font.pointSize
			)
			
			let dataWithFormat = """
			<html>
			<head>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<style> body { font-size: 150%; } </style>
			</head>
			<body>
			\(format)
			</body>
			</html>
			"""
			
			self.webView.loadHTMLString(dataWithFormat, baseURL: nil)
			self.webView.allowsBackForwardNavigationGestures = true
		}
	}
}

extension WebViewViewController: WKNavigationDelegate {
	func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
		// Start Loading
		MIIndicatorPresenter().present()
	}
	
	func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
		// Finish Loading
		MIIndicatorPresenter().hide()
	}
}
